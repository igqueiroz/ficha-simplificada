import React from 'react';
import color from "../models/colors"

let ThemeContext
const { Provider, Consumer } = ThemeContext = React.createContext();

/** Cria contexto para guardar as cores relacionadas a IES ativa. */
function ThemeProvider({ children }) {
    return (
        <Provider value={ color(process.env.REACT_APP_IES)}>
            { children }
        </Provider>
    );
}

export { ThemeProvider, Consumer as ThemeConsumer, ThemeContext }