import React from 'react';
import PropTypes from 'prop-types';

let IESContext
const { Provider, Consumer } = IESContext = React.createContext()

function IESProvider({ children }) {
    return (
        <Provider value={{ ies: process.env.REACT_APP_IES }}>
            { children }
        </Provider>
    );
}

export { IESProvider, Consumer as IESConsumer, IESContext }