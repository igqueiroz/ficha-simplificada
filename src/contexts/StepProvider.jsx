import React from 'react';
import data from "../models/data"
import { dataLayerEvent } from "../utils/gtm"

let StepContext
const { Provider, Consumer } = StepContext = React.createContext();

/** Cria contexto para guardar todos os dados de texto refernetes a IES ativa. */
class StepProvider extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            step: 0,
            totalSteps: 0
        }
        this.avancar = this.avancar.bind(this)
        this.voltar = this.voltar.bind(this)
    }

    componentDidMount() {
        const dataModel = data(process.env.REACT_APP_IES)
        const totalSteps = dataModel.form.length + 1
        this.setState({ totalSteps })
    }

    /** Incrementa o atual step do form em 1. É possível passar um callback a ser realizado
     * após atualização do step.
     * @param {boolean} condition - Condição para que o step seja atualizado. Se for null, o
     * step será atualizado.
     * @param {function} callback - Função de callbac a ser executada após avanço do estado.
    */
    avancar(condition=true, callback) {
        if( condition && this.state.step < this.state.totalSteps ) {
            dataLayerEvent(this.state.step+1)
            this.setState( currentState => ({ step: currentState.step+1 }), () => callback )
        }
    }

    /** Incrementa o atual step do form em 1. É possível passar um callback a ser realizado
     * após atualização do step.
     * @param {boolean} condition - Condição para que o step seja atualizado. Se for null, o
     * step será atualizado.
     * @param {function} callback - Função de callbac a ser executada após avanço do estado.
    */
    voltar(condition=true, callback) {
        condition && this.state.step > 0 &&
        this.setState( currentState => ({ step: currentState.step-1 }), () => callback )
    }

    render() {
        return (
            <Provider value={{
                ...this.state,
                avancar: this.avancar,
                voltar: this.voltar
            }}>
                { this.props.children }
            </Provider>
        );
    }
}

export { StepProvider, Consumer as StepConsumer, StepContext }