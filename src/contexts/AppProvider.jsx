import React from 'react';
import { DataProvider } from "./DataProvider"
import {IESProvider} from "./IESProvider"
import {ThemeProvider} from "./ThemeProvider"
import {LeadDataProvider} from "./LeadDataProvider"
import {StepProvider} from "./StepProvider"
import {SendDataProvider} from "./SendDataProvider"

/** Aglutina todos os contexts para passar para o elemento App.js */
export default function AppProvider({ children }) {
    return (
        <React.Fragment>
            <DataProvider>
                <IESProvider>
                    <ThemeProvider>
                        <LeadDataProvider>
                            <StepProvider>
                                <SendDataProvider>
                                        { children }
                                </SendDataProvider>
                            </StepProvider>
                        </LeadDataProvider>
                    </ThemeProvider>
                </IESProvider>
            </DataProvider>
        </React.Fragment>
    );
}