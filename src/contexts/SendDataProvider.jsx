import React from 'react';
import PropTypes from 'prop-types';
import http from "../utils/requestHandler"
import { normalizeData } from '../utils/postHandler';
import withLeadData from '../hocs/withLeadData'

let SendDataContext;
const { Provider, Consumer } = SendDataContext = React.createContext();

class SendDataProvider extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            id: null
        };
        this.postData = this.postData.bind(this)
    }

    postData(data, callback) {
        this.setState(
            currentState => ({ isLoading: true}), 
            () => http.post( "/inscricao", normalizeData({ ...data, evento: "inscricao" }))
                .then( res => {
                    this.setState({ id: res.data.id });
                    callback()
                })
                .catch(error => console.log(error))
                .finally(() => this.setState(currentState => ({ isLoading: false })))
        )
    }

    render() {
        return (
            <Provider value={{ ...this.state, postData: this.postData }}>
                { this.props.children }
            </Provider>
        );
    }
}

export { SendDataProvider, Consumer as SendDataConsumer, SendDataContext }