import React from 'react';
import PropTypes from 'prop-types';

let LeadDataContext;
const { Provider, Consumer } = LeadDataContext = React.createContext()

class LeadDataProvider extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.updateLeadData = this.updateLeadData.bind(this)
        this.getLeadAttribute = this.getLeadAttribute.bind(this)
    }

    /** Atualiza o objeto que contém todas as informações do usuário. É possível passar
     * uma callback function para ser executada após a atualização do objeto. Depois de
     * executada irá gerar a seguinte chave no objeto: { [name]: value }
     * @param {object} leadData = Objeto contendo duas chaves.
     * @param {object} leadData.name - Nome do atributo a ser gravado no objeto._
     * @param {object} leadData.value - Valor do atributo a ser gravado. 
     */
    updateLeadData({ name, value }, callback) {
        this.setState( currentState => ({ [name]: value }), () => callback )
    }

    /** Pega o valor de um atributo do formulário. O atributo é um campo de
     * input já preenchido pelo usuário.
     * @returns {string|number|boolean} - Retorna o tipo de dadoq ue está 
     * armazenado na chave solicitada do objeto.
     */
    getLeadAttribute(attribute) {
        return this.state[attribute]
    }

    render() {
        return (
            <Provider value={{ 
                leadData: { ...this.state },
                updateLeadData: this.updateLeadData,
                getLeadAttribute: this.getLeadAttribute
            }}>
                { this.props.children }
            </Provider>
        );
    }
}

export { LeadDataProvider, Consumer as LeadDataConsumer, LeadDataContext }