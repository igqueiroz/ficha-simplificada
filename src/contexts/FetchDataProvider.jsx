import React from "react"   

let FetchDataContext
const { Provider, Consumer } = FetchDataContext = React.createContext()

class FetchDataProvider extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
        this.updateData = this.updateData.bind(this)
    }

    updateData(name, data) {
        this.setState({ [name]: data })
    }

    render() {
        return(
            <Provider value={{ ...this.state, updateData: this.updateData }}>
                { this.props.children }
            </Provider>
        )
    }
}

export { FetchDataProvider, Consumer as FetchDataConsumer, FetchDataContext }