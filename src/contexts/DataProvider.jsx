import React from 'react';
import data from "../models/data"

let DataContext
const { Provider, Consumer } = DataContext = React.createContext();

/** Cria contexto para guardar todos os dados de texto refernetes a IES ativa. */
function DataProvider({ children }) {
    return (
        <Provider value={ data(process.env.REACT_APP_IES) }>
            { children }
        </Provider>
    );
}

export { DataProvider, Consumer as DataConsumer, DataContext }