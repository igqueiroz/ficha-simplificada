import UNIFACS from "./ies/unf-model"
import UNP from "./ies/unp-model"
import UNN from "./ies/unn-model"
import UNIFG from "./ies/ufg-model"
import FPB from "./ies/fpb-model"
import IBMR from "./ies/ibm-model"
import EAD from "./ies/ead-model"
import UAM from "./ies/uam-model"
import FMU from "./ies/fmu-model"
import FADERGS from "./ies/fad-model"
import UNIRITTER from "./ies/unr-model"

export default ies => {
    switch(ies) {
        case "ead":
            return EAD
         case "unf":
            return UNIFACS
        case "unn":
            return UNN
        case "unp":
            return UNP
        case "ufg":
            return UNIFG
        case "fpb":
            return FPB
        case "ibm":
            return IBMR
        case "uam":
            return UAM
        case "fmu":
            return FMU
        case "fad":
            return FADERGS
        case "unr":
            return UNIRITTER
    }
}