export default {
    inscricao: [
        { info: "Nome", param: "nome" },
        { info: "Nota do ENEM", param: "notaEnem" },
        { info: "Curso", param: "nomeCurso" },
        { info: "Campus", param: "campusPolo" },
        { info: "Período", param: "modalidade" },
        { info: "Turno", param: "turno" }
    ],
    chamada: "Descubra quanto vale a sua bolsa",
    botao: {
        texto: "Quero minha bolsa!!",
        actions: []
    },
    campus: [
        {
            name: 'Sertório', value: 'Sertório',
            endereco: 'Av. Sertório, 5310, Jardim Floresta - Porto Alegre/RS - CEP 90670-002',
            horario: 'Seg - Sex (09h às 21h30) Sáb (09h às 12h)',
            telefone: '0800 729 0779'
        },
        {
            name: 'Galeria Luza', value: 'Galeria Luza',
            endereco: 'Rua Marechal Floriano Peixoto, 185, Centro Histórico - Porto Alegre/RS - CEP 90020-061',
            horario: 'Seg - Sex (09h às 21h30) Sáb (09h às 12h)',
            telefone: '0800 729 0779'
        },
    ],
    agendamento: {
        mensagem: "Você deve receber um email com as informações de agendamento."
    }
}