export default {
    inscricao: [
        { info: "Nome", param: "nome" },
        { info: "Curso", param: "nomeCurso" },
        { info: "Cidade", param: "cidade" },
        { info: "Período", param: "turno" },
        { info: "MOdalidade", param: "modalidade" }
    ],
    chamada: "Descubra quanto vale a sua bolsa",
    botao: {
        texto: "Agende sua matrícula com desconto",
        actions: []
    },
    campus: [
        {
            name: 'Salvador 1', value: 'Salvador 1',
            endereco: '',
            horario: '',
            telefone: ''
        },
        {
            name: 'Salvador 2', value: 'Salvador 2',
            endereco: '',
            horario: '',
            telefone: ''
        },
        {
            name: 'Salvador 3', value: 'Salvador 3',
            endereco: '',
            horario: '',
            telefone: ''
        },
        {
            name: 'Feira de Santana', value: 'Feira de Santana',
            endereco: '',
            horario: '',
            telefone: ''
        },
    ]
}