export default {
    inscricao: [
        { info: "Nome", param: "nome" },
        { info: "Curso", param: "nomeCurso" },
        { info: "Período", param: "turno" },
        { info: "MOdalidade", param: "modalidade" }
    ],
    chamada: "Descubra quanto vale a sua bolsa",
    botao: {
        texto: "Agende sua matrícula com desconto",
        actions: []
    },
    campus: [
        {
            name: 'Mooca', value: 'Mooca',
            endereco: '',
            horario: '',
            telefone: ''
        },
        {
            name: 'Vila Olímpia', value: 'Vila Olímpia',
            endereco: '',
            horario: '',
            telefone: ''
        },
        {
            name: 'Paulista 1', value: 'Paulista 1',
            endereco: '',
            horario: '',
            telefone: ''
        }
    ]
}