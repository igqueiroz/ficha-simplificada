export default {
    inscricao: [
        { info: "Nome", param: "nome" },
        { info: "Nota do ENEM", param: "notaEnem" },
        { info: "Curso", param: "nomeCurso" },
        { info: "Campus", param: "campusPolo" },
        { info: "Turno", param: "valueTurno" },
    ],
    chamada: "Descubra quanto vale a sua bolsa",
    botao: {
        texto: "QUERO MINHA BOLSA",
        actions: []
    },
    campus: [
        {
            name: 'Canoas', value: 'Canoas',
            endereco: 'Rua Santos Dumont, 888, Niterói - Canoas/RS - CEP 92120-110',
            horario: 'Seg - Sex (09h às 21h30)   Sáb (09h às 12h)',
            telefone: '0800 881 0001'
        },
        {
            name: 'Zona Sul', value: 'Zona Sul',
            endereco: 'Rua Orfanotrófio, 555, Alto Teresópolis - Porto Alegre/RS - CEP 90840-440',
            horario: 'Seg - Sex (09h às 21h30) Sáb (09h às 12h)',
            telefone: '0800 881 0001'
        },
        {
            name: 'FAPA', value: 'FAPA',
            endereco: 'Av. Manoel Elias, 2001, Passo das Pedras - Porto Alegre/RS - CEP 91240-261',
            horario: 'Seg - Sex (09h às 21h30) Sáb (09h às 12h)',
            telefone: '0800 881 0001'
        },
        {
            name: 'Iguatemi', value: 'Iguatemi',
            endereco: "Av. João Wallig, 1800, Passo d'Areia - Porto Alegre/RS - CEP 91340-000",
            horario: 'Seg - Sex (09h às 21h30) Sáb (09h às 12h)',
            telefone: '0800 881 0001'
        }
    ],
    agendamento: {
        mensagem: "Você deve receber um email com as informações de agendamento.",
    }
}