export default {
    inscricao: [
        { info: "Nome", param: "nome" },
        { info: "Nota do ENEM", param: "notaEnem" },
        { info: "Curso", param: "nomeCurso" },
        { info: "Campus", param: "campusPolo" },
        { info: "Período", param: "modalidade" },
        { info: "Turno", param: "turno" }
    ],
    chamada: "Agende agora sua matrícula para descobrir seu desconto!",
    botao: {
        texto: "Quero agendar minha matrícula!",
        actions: []
    },
    campus: [
        {
            name: 'Recife', value: 'Recife',
            endereco: 'Av. Governador Carlos de Lima Cavalcante, 155 - Boa Vista, Recife - PE, Brasil',
            horario: '8:00 às 21:00',
            telefone: '(81) 4020-9085'
        },
        {
            name: 'Piedade', value: 'Piedade',
            endereco: 'Av. Barão de Lucena, 99 - Centro, Jaboatão dos Guararapes - PE, Brasil',
            horario: '8:00 às 21:00',
            telefone: '(81) 4020-9085'
        },
        {
            name: 'Jaboatão dos Guararapes', value: 'Jaboatão dos Guararapes',
            endereco: 'Rua Comendador José Didiê, 27 - Piedade, Jaboatão dos Guararapes - PE, Brasil',
            horario: '8:00 às 21:00',
            telefone: '(81) 4020-9085'
        },
    ]
}