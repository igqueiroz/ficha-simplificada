export default {
    inscricao: [
        { info: "Nome", param: "nome" },
        { info: "Nota do ENEM", param: "notaEnem" },
        { info: "Curso", param: "nomeCurso" },
        { info: "Unidade", param: "campusPolo" },
        { info: "Modalidade", param: "modalidade" },
        { info: "Turno", param: "turno" }
    ],
    chamada: "Descubra quanto vale a sua bolsa",
    botao: {
        texto: "Agende sua matrícula com desconto",
        actions: []
    },
    campus: [
        {
            name: 'Natal - Unidade Roberto Freire', value: 'Natal - Unidade Roberto Freire',
            endereco: 'Av. Eng. Roberto Freire, 2184, Capim Macio.',
            horario: 'Segunda a sexta, das 9h às 21h, e sábado, das 8h às 14h.',
            telefone: '(84) 4020-7890'
        },
        {
            name: 'Natal - Unidade Salgado Filho', value: 'Natal - Unidade Salgado Filho',
            endereco: 'Av. Sen. Salgado Filho, 1610, Lagoa Nova.',
            horario: 'Segunda a sexta, das 9h às 21h, e sábado, das 8h às 14h.',
            telefone: '(84) 4020-7890'
        },
        {
            name: 'Natal - Unidade Zona Norte', value: 'Natal - Unidade Zona Norte',
            endereco: 'Av. João Medeiros Filho, 2300, Potengi.',
            horario: 'Segunda a sexta, das 9h às 21h, e sábado, das 8h às 14h.',
            telefone: '(84) 4020-7890'
        },
        {
            name: 'Campus Mossoró', value: 'Campus Mossoró',
            endereco: 'Av. João da Escóssia, 1561, Nova Betânia.',
            horario: 'Segunda a sexta, das 9h às 21h, e sábado, das 8h às 14h.',
            telefone: '(84) 4020-7890'
        },
        {
            name: 'Polo Caicó', value: 'Polo Caicó',
            endereco: 'Rua Otávio Lamartine, 461, Centro.',
            horario: 'Segunda a sexta, das 9h às 21h, e sábado, das 8h às 14h.',
            telefone: '(84) 4020-7890'
        },
        {
            name: 'Polo Currais Novos', value: 'Polo Currais Novos',
            endereco: 'Praça Cristo Rei, 74, Centro.',
            horario: 'Segunda a sexta, das 9h às 21h, e sábado, das 8h às 14h.',
            telefone: '(84) 4020-7890'
        },
        {
            name: 'Polo Pau dos Ferros', value: 'Polo Pau dos Ferros',
            endereco: 'R. Alexandre Pinto, 743, Pau dos Ferros',
            horario: 'Segunda a sexta, das 9h às 21h, e sábado, das 8h às 14h.',
            telefone: '(84) 4020-7890'
        },
        {
            name: 'Polo Limoeiro do Norte - CE', value: 'Polo Limoeiro do Norte - CE',
            endereco: 'R. Sabino Roberto de Freitas, 3098 - Centro.',
            horario: 'Segunda a sexta, das 9h às 21h, e sábado, das 8h às 14h.',
            telefone: '(84) 4020-7890'
        },
    ]
}