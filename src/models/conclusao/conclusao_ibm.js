export default {
    inscricao: [
        { info: "Nome", param: "nome" },
        { info: "Nota do ENEM", param: "notaEnem" },
        { info: "Curso", param: "nomeCurso" },
        { info: "Campus", param: "campusPolo" },
        { info: "Modalidade", param: "modalidade" },
        { info: "Turno", param: "turno" }
    ],
    chamada: "Descubra quanto vale a sua bolsa",
    botao: {
        texto: "Agende sua matrícula com desconto",
        actions: []
    },
    campus: [
        {
            name: 'Barra da Tijuca', value: 'Barra da Tijuca',
            endereco: '',
            horario: '',
            telefone: ''
        },
        {
            name: 'Botafogo', value: 'Botafogo',
            endereco: '',
            horario: '',
            telefone: ''
        },
        {
            name: 'Centro', value: 'Centro',
            endereco: '',
            horario: '',
            telefone: ''
        },
        {
            name: 'Polo Nova Iguaçu', value: 'Polo Nova Iguaçu',
            endereco: '',
            horario: '',
            telefone: ''
        }
    ],
    agendamento: {
        mensagem: "Você deve receber um email com as informações de agendamento."
    }
}