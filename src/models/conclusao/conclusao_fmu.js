export default {
    inscricao: [
        { info: "Nome", param: "nome" },
        { info: "Nota do ENEM", param: "notaEnem" },
        { info: "Curso", param: "nomeCurso" },
        { info: "Modalidade", param: "modalidade" },
        { info: "Campus", param: "campusPolo" },
        { info: "Turno", param: "valueTurno" },
    ],
    chamada: "Descubra quanto vale a sua bolsa",
    botao: {
        texto: "AGENDAR VESTIBULAR",
        action: () => window.location.href="https://movemais.fmu.br/inscricao_agendado/?_ga=2.89103643.1696924874.1547725290-337412341.1534795829"
    }
}