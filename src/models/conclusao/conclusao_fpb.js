export default {
    inscricao: [
        { info: "Nome", param: "nome" },
        { info: "Curso", param: "nomeCurso" },
        { info: "Modalidade", param: "modalidade" },
        { info: "Campus", param: "campusPolo" },
        { info: "Turno", param: "turno" }
    ],
    chamada: "Descubra quanto vale a sua bolsa",
    botao: {
        texto: "Agende sua matrícula com desconto!",
        actions: []
    },
    campus: [
        {
            name: 'João Pessoa', value: 'João Pessoa',
            endereco: 'Av. Monsenhor Walfredo Leal, 512 - Tambiá - João Pessoa/PB',
            horario: 'Segunda a sexta-feira das 8:30 às 21:00',
            telefone: '(83) 4020-9084'
        },
    ],
    agendamento: {
        mensagem: "Você deve receber um email com as informações de agendamento."
    }
}