export default [
    { oferta: "30%", range: "De 200 a 550" },
    { oferta: "35%", range: "De 551 a 669" },
    { oferta: "40%", range: "De 700 a 749" },
    { oferta: "50%", range: "De 750 a 799" },
    { oferta: "60%", range: "De 800 a 900" },
    { oferta: "100%", range: "A partir de 901" },
]