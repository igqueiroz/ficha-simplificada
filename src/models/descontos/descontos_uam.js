export default [
    { oferta: "20%", range: "De 400 a 449" },
    { oferta: "30%", range: "De 450 a 499" },
    { oferta: "40%", range: "De 500 a 699" },
    { oferta: "50%", range: "De 600 a 799" },
    { oferta: "100%", range: "A partir de 800" }
]