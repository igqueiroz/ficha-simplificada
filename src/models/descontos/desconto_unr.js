export default [
    { oferta: "30%", range: "De 300 a 400" },
    { oferta: "40%", range: "De 401 a 500" },
    { oferta: "50%", range: "De 501 a 600" },
    { oferta: "60%", range: "De 601 a 650" },
    { oferta: "70%", range: "De 651 a 800" },
    { oferta: "90%", range: "De 801 a 900" },
    { oferta: "100%", range: "A partir de 901" },
]