export default [
    { oferta: "17%", range: "De 200 a 300" },
    { oferta: "22%", range: "De 301 a 400" },
    { oferta: "25%", range: "De 401 a 500" },
    { oferta: "30%", range: "De 501 a 600" },
    { oferta: "35%", range: "De 601 a 700" },
    { oferta: "40%", range: "De 701 a 800" },
    { oferta: "45%", range: "A partir de 801" },
]