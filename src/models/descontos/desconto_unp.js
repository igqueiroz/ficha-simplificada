export default [
    { oferta: "40%", range: "De 200 a 400" },
    { oferta: "45%", range: "De 401 a 600" },
    { oferta: "50%", range: "De 601 a 700" },
    { oferta: "55%", range: "De 701 a 800" },
    { oferta: "70%", range: "De 801 a 900" },
    { oferta: "100%", range: "A partir de 901" },
]