export default [
    {
        component: "select",
        name: "cidade",
        id: "cidade",
        width: "100%",
        placeholder: "Cidade",
        endpoint: "cidade"
    },

    {
        component: "drawer",
        name: "valueCurso",
        alias: "nomeCurso",
        id: "curso",
        width: "47.5%",
        renderDataAs: "list",
        placeholder: "Escolha s eu curso",
        endpoint: "curso/:cidade",
        refetch: "cidade"
    },

    {
        component: "select",
        name: "modalidade",
        id: "modalidade",
        placeholder: "Modalidade",
        width: "47.5%",
        endpoint: ":valueCurso/modalidade",
        refetch: "valueCurso"
    },

    {
        component: "drawer",
        name: "valueCampusPolo",
        alias: "campusPolo",
        id: "campus",
        placeholder: "Campus",
        renderDataAs: "radio",
        width: "47.5%",
        endpoint: ":valueCurso/:modalidade/campus",
        refetch: "valueCurso"
    },

    {
        component: "select",
        name: "valueTurno",
        alias: "turno",
        id: "turno",
        placeholder: "Turno",
        width: "47.5%",
        endpoint: ":valueCurso/:valueCampusPolo/turno",
        refetch: "campusPolo"
    },

    {
        component: "buttonVoltar",
        name: "avancar",
        actions: ["voltar"],
        text: "Voltar"
    },

    {
        component: "buttonProximo",
        name: "avancar",
        actions: ["avancar"],
        text: "Próximo",
        shouldBlock: true
    }
]