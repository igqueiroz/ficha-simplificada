export default [
    {
        component: "input",
        name: "notaEnem",
        id: "notaEnem",
        placeholder: "Nota do Enem (Informação opcional)",
        width: "47.5%",
        mask: [ /\d/, /\d/, /\d/, ".", /\d/, /\d/ ],
        guide: false
    },

    {
        component: "input",
        name: "anoEnem",
        id: "anoEnem",
        width: "47.5%",
        placeholder: "Nota da redação (Informação opcional)",
        mask: [ /\d/, /\d/, /\d/, ".", /\d/, /\d/ ],
        guide: false
    },

    {
        component: "drawer",
        name: "valueCurso",
        alias: "nomeCurso",
        id: "curso",
        width: "47.5%",
        renderDataAs: "list",
        placeholder: "Escolha seu curso",
        endpoint: "curso",
        required: true,
        render: "select"
    },

    {
        component: "select",
        name: "modalidade",
        id: "modalidade",
        width: "47.5%",
        placeholder: "Modalidade",
        endpoint: ":valueCurso/modalidade",
        refetch: "valueCurso",
        required: true
    },

    {
        component: "drawer",
        name: "valueCampusPolo",
        alias: "campusPolo",
        id: "campus",
        placeholder: "Campus",
        renderDataAs: "radio",
        width: "47.5%",
        endpoint: ":valueCurso/:modalidade/campus",
        refetch: "modalidade",
        required: true,
        render: "select"
    },

    {
        component: "select",
        name: "valueTurno",
        alias: "turno",
        id: "turno",
        placeholder: "Turno",
        width: "47.5%",
        endpoint: ":valueCurso/:valueCampusPolo/turno",
        refetch: "valueCampusPolo",
        required: true
    },

    {
        component: "buttonVoltar",
        name: "voltar",
        actions: ["voltar"],
        text: "Voltar",
        widthMobile: "60px"
    },

    {
        component: "buttonProximo",
        name: "avancar",
        actions: ["postData"],
        text: "Quero minha vaga!",
        shouldBlock: true
    }
]