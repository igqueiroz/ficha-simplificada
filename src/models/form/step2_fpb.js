export default [
    {
        component: "drawer",
        name: "valueCurso",
        alias: "nomeCurso",
        id: "curso",
        width: "47.5%",
        renderDataAs: "list",
        placeholder: "Escolha seu curso",
        endpoint: "curso",
        required: true,
        render: "select"
    },

    {
        component: "select",
        name: "modalidade",
        id: "modalidade",
        width: "47.5%",
        placeholder: "Modalidade",
        endpoint: ":valueCurso/modalidade",
        refetch: "valueCurso",
        required: true
    },

    {
        component: "drawer",
        name: "valueCampusPolo",
        alias: "campusPolo",
        id: "campus",
        placeholder: "Campus",
        renderDataAs: "radio",
        width: "47.5%",
        endpoint: ":valueCurso/campus",
        refetch: "modalidade",
        required: true,
        render: "select"
    },

    {
        component: "select",
        name: "valueTurno",
        alias: "turno",
        id: "turno",
        placeholder: "Turno",
        width: "47.5%",
        endpoint: ":valueCurso/:valueCampusPolo/turno",
        refetch: "valueCampusPolo",
        required: true
    },

    {
        component: "buttonVoltar",
        name: "voltar",
        actions: ["voltar"],
        text: "Voltar"
    },

    {
        component: "buttonProximo",
        name: "avancar",
        actions: ["postData"],
        text: "Próximo",
        shouldBlock: true
    }
]