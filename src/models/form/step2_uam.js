export default [
    {
        component: "drawer",
        name: "valueCurso",
        alias: "nomeCurso",
        id: "curso",
        width: "47.5%",
        renderDataAs: "list",
        placeholder: "Escolha seu curso",
        endpoint: "curso",
        required: true
    },

    {
        component: "select",
        name: "modalidade",
        id: "modalidade",
        width: "47.5%",
        placeholder: "Modalidade",
        endpoint: ":valueCurso/modalidade",
        refetch: "valueCurso",
        required: true
    },

    {
        component: "drawer",
        name: "valueCampusPolo",
        alias: "campusPolo",
        id: "campus",
        placeholder: "Campus",
        renderDataAs: "radio",
        width: "47.5%",
        endpoint: ":valueCurso/campus",
        refetch: "valueCurso",
        required: true
    },

    {
        component: "select",
        name: "valueTurno",
        alias: "turno",
        id: "turno",
        placeholder: "Turno",
        width: "47.5%",
        endpoint: ":valueCurso/:valueCampusPolo/turno",
        refetch: "valueCampusPolo",
        required: true
    },

    {
        component: "input",
        name: "rg",
        id: "rg",
        placeholder: "RG",
        width: "47.5%",
        mask: false,
        required: true
    },
    
    {
        component: "input",
        name: "nascimento",
        id: "nascimento",
        placeholder: "Data de nascimento",
        width: "47.5%",
        mask: [/\d/, /\d/, "/", /\d/, /\d/, "/", /\d/, /\d/, /\d/, /\d/ ],
        required: true
    },

    {
        component: "buttonVoltar",
        name: "voltar",
        actions: ["voltar"],
        text: "Voltar"
    },

    {
        component: "buttonProximo",
        name: "avancar",
        actions: ["postData"],
        text: "Próximo",
        shouldBlock: true
    }
]