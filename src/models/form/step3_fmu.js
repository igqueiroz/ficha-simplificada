export default [
    {
        component: "input",
        name: "nomeMae",
        id: "nomeMae",
        placeholder: "Nome da mãe",
        width: "100%",
        guide: false,
        mask: [ /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/, /[a-zA-Z ]/,/[a-zA-Z ]/,/[a-zA-Z ]/,/[a-zA-Z ]/,/[a-zA-Z ]/,/[a-zA-Z ]/,/[a-zA-Z ]/,/[a-zA-Z ]/,/[a-zA-Z ]/,/[a-zA-Z ]/,/[a-zA-Z ]/,/[a-zA-Z ]/,/[a-zA-Z ]/,/[a-zA-Z ]/,/[a-zA-Z ]/,/[a-zA-Z ]/,/[a-zA-Z ]/,/[a-zA-Z ]/,/[a-zA-Z ]/,/[a-zA-Z ]/,/[a-zA-Z ]/,/[a-zA-Z ]/ ],
        required: true
    },

    {
        component: "input",
        name: "rg",
        id: "rg",
        placeholder: "RG",
        width: "47.5%",
        mask: false,
        required: true
    },

    {
        component: "input",
        name: "nascimento",
        id: "nascimento",
        placeholder: "Data de nascimento",
        width: "47.5%",
        mask: [/\d/, /\d/, "/", /\d/, /\d/, "/", /\d/, /\d/, /\d/, /\d/ ],
        required: true
    },

    {
        component: "select",
        name: "sexo",
        id: "sexo",
        placeholder: "Sexo",
        width: "47.5%",
        options: [{ value: "Sexo-1", name: "Masculino" }, { value: "Sexo-2", name: "Feminino" }]
    },

    {
        component: "input",
        name: "anoRealizacao",
        id: "anoRealizacao",
        placeholder: "Ano de conclusão do Ensino Médio",
        width: "47.5%",
        guide: false,
        mask: [/\d/, /\d/, /\d/, /\d/],
        required: true
    },

    {
        component: "input",
        name: "cep",
        id: "cep",
        placeholder: "CEP",
        width: "47.5%",
        guide: false,
        mask: [/\d/, /\d/, ".", /\d/, /\d/, /\d/, "-", /\d/, /\d/, /\d/ ],
        required: true
    },

    {
        component: "input",
        name: "numero",
        id: "numero",
        placeholder: "Número da residência",
        width: "47.5%",
        mask: [/[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/ ],
        guide: false,
        required: true
    },

    {
        component: "buttonVoltar",
        name: "avancar",
        actions: ["voltar"],
        text: "Voltar"
    },

    {
        component: "buttonProximo",
        name: "avancar",
        actions: ["postData"],
        text: "Quero minha vaga!",
        shouldBlock: true
    }
]