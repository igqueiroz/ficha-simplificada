export default [
    {
        component: "select",
        name: "estado",
        id: "estado",
        width: "47.5%",
        placeholder: "Estado",
        endpoint: "estado",
        required: true
    },

    {
        component: "select",
        name: "cidade",
        id: "cidade",
        width: "47.5%",
        placeholder: "Cidade",
        endpoint: ":estado/cidade",
        refetch: "estado",
        required: true
    },

    {
        component: "select",
        name: "polo",
        id: "polo",
        width: "47.5%",
        placeholder: "Polo",
        endpoint: ":cidade/polo",
        refetch: "cidade",
        required: true
    },

    {
        component: "select",
        name: "valueCurso",
        alias: "nomeCurso",
        id: "curso",
        width: "47.5%",
        placeholder: "Polo",
        endpoint: "curso",
        required: true
    },

    {
        component: "input",
        name: "anoRealizacao",
        id: "curso",
        width: "47.5%",
        placeholder: "Ano de Realização do ENEM",
        required: true
    },

    {
        component: "buttonVoltar",
        name: "voltar",
        actions: ["voltar"],
        text: "Voltar"
    },

    {
        component: "buttonProximo",
        name: "avancar",
        actions: ["postData"],
        text: "Próximo",
        shouldBlock: true
    }
]