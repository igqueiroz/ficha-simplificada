export default [
    {
        component: "input",
        type: "text",
        name: "nome",
        id: "nome",
        placeholder: "Nome Completo",
        guide: false,
        mask: false,
        required: true
    },

    {
        component: "input",
        type: "text",
        name: "telefone",
        id: "telefone",
        placeholder: "Celular",
        mask: [ '('  , /[0-9]/ , /[0-9]/ , ')' , ' ' , /[0-9]/ , ' ' , /[0-9]/ , /[0-9]/ , /[0-9]/ , /[0-9]/ , '-' , /[0-9]/ , /[0-9]/ , /[0-9]/ , /[0-9]/ ],
        guide: false,
        required: true
    },

    {
        component: "input",
        type: "text",
        name: "email",
        id: "email",
        placeholder: "E-mail",
        mask: false,
        required: true
    },

    {
        component: "input",
        type: "text",
        name: "cpf",
        id: "cpf",
        placeholder: "CPF",
        mask: [ /[0-9]/ , /[0-9]/ , /[0-9]/ , '.' , /[0-9]/ , /[0-9]/ , /[0-9]/ , '.' , /[0-9]/ , /[0-9]/ , /[0-9]/ , '-' , /[0-9]/ , /[0-9]/ ],
        required: true
    },

    {
        component: "buttonInicial",
        name: "avancar",
        actions: ["avancar"],
        text: "Próximo",
        shouldBlock: true
    }
]