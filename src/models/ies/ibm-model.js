import step1 from "../form/step1"
import step2 from "../form/step2"
import conclusao from "../conclusao/conclusao_ibm"
import descontos from "../descontos/desconto_ibm"

export default {
    basicData: {
        name: "a IBMR",
        url: 'https://www.ibmr.br/'
    },
    form: [ step1, step2 ],
    conclusao,
    descontos
}