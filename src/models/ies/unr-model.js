import step1 from "../form/step1"
import step2 from "../form/step2_unr_fad"
import conclusao from "../conclusao/conclusao_unr"
import descontos from "../descontos/desconto_unr"

export default {
    basicData: {
        name: "a UniRitter",
        url: 'https://uniritter.edu.br'
    },
    form: [ step1, step2 ],
    conclusao,
    descontos
}