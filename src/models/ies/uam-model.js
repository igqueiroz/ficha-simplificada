import step1 from "../form/step1"
import step2 from "../form/step2_uam"
import conclusao from "../conclusao/conclusao_uam"
import descontos from "../descontos/descontos_uam"

export default {
    basicData: {
        name: "a Anhembi Morumbi",
        url: 'https://portal.anhembi.br/'
    },
    form: [ step1, step2 ],
    conclusao,
    descontos
}