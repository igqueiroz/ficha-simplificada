import step1 from "../form/step1"
import step2 from "../form/step2_fmu"
import step3 from "../form/step3_fmu"
import conclusao from "../conclusao/conclusao_fmu"
import descontos from "../descontos/desconto_fmu"

export default {
    basicData: {
        name: "a FMU",
        url: 'https://fmu.br'
    },
    form: [ step1, step2, step3 ],
    conclusao,
    descontos
}