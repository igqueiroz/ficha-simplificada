import step1 from "../form/step1"
import step2 from "../form/step2"
import conclusao from "../conclusao/conclusao_unp"
import descontos from "../descontos/desconto_unp"

export default {
    basicData: {
        name: "à UnP.",
        url: 'https://unp.br/'
    },
    form: [ step1, step2 ],
    conclusao,
    descontos
}