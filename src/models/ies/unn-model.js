import step1 from "../form/step1"
import step2 from "../form/step2"

export default {
    basicData: {
        name: "a UniNorte",
        url: 'https://www.uninorte.com.br/'
    },
    form: [ step1, step2 ]
}