import step1 from "../form/step1"
import step2 from "../form/step2_unf"
import conclusao from "../conclusao/conclusao_unf"
import descontos from "../descontos/descontos_unf"

export default {
    basicData: {
        name: "a UNIFACS",
        url: 'https://www.unifacs.br/'
    },
    form: [ step1, step2 ],
    conclusao,
    descontos
}