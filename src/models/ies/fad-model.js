import step1 from "../form/step1"
import step2 from "../form/step2_unr_fad"
import conclusao from "../conclusao/conclusao_fad"
import descontos from "../descontos/desconto_fad"

export default {
    basicData: {
        name: "a FADERGS",
        url: 'https://fadergs.edu.br',
    },
    form: [ step1, step2 ],
    conclusao,
    descontos
}