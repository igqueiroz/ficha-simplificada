import step1 from "../form/step1"
import step2 from "../form/step2_fpb"
import conclusao from "../conclusao/conclusao_fpb"
import descontos from "../descontos/desconto_fpb"

export default {
    basicData: {
        name: "a FPB",
        url: 'https://www.fpb.edu.br'
    },
    form: [ step1, step2 ],
    conclusao,
    descontos
}