import React from 'react';
import PropTypes from 'prop-types';
import { LeadDataConsumer } from "../contexts/LeadDataProvider"

export default function withLeadData(Component) {
    return function withLeadData(props) {
        return (
            <LeadDataConsumer>
                {({ leadData, updateLeadData }) => (
                    <Component {...props} leadData={leadData} updateLeadData={updateLeadData} />
                )}
            </LeadDataConsumer>
        );
    }
}