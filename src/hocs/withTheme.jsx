import React from 'react';
import PropTypes from 'prop-types';
import { ThemeConsumer } from "../contexts/ThemeProvider"

/** Adiciona a props "theme" a um componente. */
export default function withTheme(Component) {
    return function withTheme(props) {
        return (
            <ThemeConsumer>
                {theme => (
                    <Component {...props} theme={theme ? theme : props.theme} />
                )}
            </ThemeConsumer>
        );
    }
}