import React from 'react';
import PropTypes from 'prop-types';
import { StepConsumer } from "../contexts/StepProvider"
import { LeadDataConsumer } from "../contexts/LeadDataProvider"

export default function withAbandono(Component) {
    return class withAbandono extends React.Component {
        constructor(props) {
            super(props);
            this.state = {};
            this.onUnload = this.onUnload.bind(this)
        }

        onUnload(leadData, step) {
            const token = ie => {
                switch( ie ) {
                    case 'unn':
                        return "5ba993af4cc85dd37667d8b91e94f184";
                    case 'ufg':
                        return "d1be35ae84c69fac1941a1d64214a5ab";
                    case 'fpb':
                        return "7dbf6ad00f4fe59eabb48398ea25ffc9";
                    case 'unp':
                        return "635dfc1c5acd2b88196b6ba21e1d8a75";
                    case 'unf':
                        return "a2f4118de52f0d063fc8a9f00a1b8a0b";
                    case 'uam':
                        return "04d713eec86c06caa067bfcaf866499d";
                    case 'fmu':
                        return "3470254e3b62770353646b8c97e67a65";
                    case 'ibm':
                        return "9d8769814b7242dfedb5b1b4037fd78d";
                    case 'fad':
                        return "49e6adfa85df20dc3fd43bc796c904df";
                        break;
                    case 'unr':
                        return "975cdf028ac20021ab6d845d73847ef8";
                        break;
                    case 'CE':
                        return '635dfc1c5acd2b88196b6ba21e1d8a75';
                        break;
                    default:
                        return "9b77707c7865bf2c4b2c0c054d895314";
                };
            }
            const beaconData = {
                token_rdstation: token(process.env.REACT_APP_IES),
                identificador: this.props.step.step < this.props.step.totalSteps-1 ? "ficha-wr-enem-abandono" : null,
                evento: "abandono",
            }
            const fullData = { ...leadData , ...beaconData  }
            const headers = {
                type: "application/json"
            }
            var client = new XMLHttpRequest();
            client.open("POST", "https://www.rdstation.com.br/api/1.3/conversions", false); // third parameter indicates synchronous ajax
            client.setRequestHeader("Content-Type", "application/json");
            client.send(JSON.stringify(fullData));
        }

        render() {
            return (
                <StepConsumer>
                    {step=> (
                        <LeadDataConsumer>
                            {({ leadData }) => (
                                <Component {...this.props} sendAbandono={this.onUnload} leadData={leadData} step={step} />
                            )}
                        </LeadDataConsumer>
                    )}
                </StepConsumer>
            );
        }
    }
}