import React from 'react';
import PropTypes from 'prop-types';
import http from "../utils/requestHandler"
import withLeadData from './withLeadData';
import withToken from "./withToken"
import _ from "lodash"
import Axios from '../../../../function/node_modules/axios';

const propTypes = {};

const defaultProps = {};

/** 
 * Realiza o fetch de dados para uma API e, quando necessário. a atualização do Fetch.
 * Componente ainda muito acoplado ao componente SELECT, que precisa ser atualizado
 * quando algum outro valor é trocado.
 */
export default function withRefetch(Component) {
    return withLeadData(withToken(class withRefetch extends React.Component {
        constructor(props) {
            super(props);
            this.state = { data: [] };
        }

        changeFadergsCourseName(courses) {
            if( process.env.REACT_APP_IES === "fad" ) {
                return courses.map( course => {
                    if( course.name.includes("CURSO SUPERIOR DE TECNOLOGIA EM")) {
                        return { name: course.name.replace("CURSO SUPERIOR DE TECNOLOGIA EM", "").trim(), value: course.value }
                    }
                    if( course.name.includes("BACHARELADO EM")) {
                        return { name: course.name.replace("BACHARELADO EM", "").trim(), value: course.value }
                    }
                    if( course.name.includes("GRADUAÇÃO EM")) {
                        return { name: course.name.replace("GRADUAÇÃO EM", "").trim(), value: course.value }
                    }
                    return course
                })
            }
            return courses
        }

        componentDidMount() {
            if(this.props.endpoint && !this.props.refetch && this.props.token ) {
                http.get( `/${this.props.endpoint}`, { headers: { "authorization": this.props.token }})
                    .then( res => {
                        const data = this.changeFadergsCourseName(res.data)
                        const orderedData = _.sortBy( data,  d => d.name.trim() )
                        this.setState({ data: orderedData })
                    })
            }
        }
    
        componentDidUpdate(prevProps) {
            const { refetch, leadData, endpoint, name, token } = this.props
            if(this.props.refetch && prevProps.leadData[refetch] !== leadData[refetch]) {
                console.log(this.props.name)
                //Atualiza objeto leaData para resetar valores do select ou do drawer quando o componente do
                // refetch é atualziado.
                // ---- INICIO ------
                this.props.updateLeadData({ name: this.props.name, value: "-1" })
                this.props.alias && this.props.updateLeadData({ name: this.props.alias, value: undefined })
                this.setState({ data: [] })
                // ----- FIM --------
                const url = endpoint.split("/").map( route => route.includes(":") ? leadData[route.substring(1)] : route ).join("/")
                http.get( url, { headers: { "authorization": this.props.token }})
                    .then( res => {
                        const data = this.changeFadergsCourseName(res.data)
                        const orderedData = _.sortBy( data, "name" )
                        this.setState({ data: orderedData })
                    })
            }
        }

        render() {
            return <Component {...this.props} {...this.state} />
        }
    }))
}

 withRefetch.propTypes = propTypes;
 withRefetch.defaultProps = defaultProps;