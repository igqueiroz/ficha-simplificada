import React from 'react';
import PropTypes from 'prop-types';
import http from "../utils/requestHandler"

const propTypes = {};

const defaultProps = {};

export default function withToken(Component) {
    return class withToken extends React.Component {
        constructor(props) {
            super(props);
            this.state = {};
        }

        componentDidMount() {
            !this.state.token && http.get("/token").then(res => this.setState({ token: res.data.auth }))
        }

        render() {
            return <Component {...this.props} {...this.state} />
        }
    }
}

 withToken.propTypes = propTypes;
 withToken.defaultProps = defaultProps;