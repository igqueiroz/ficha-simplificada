import React from 'react';
import PropTypes from 'prop-types';
import validator from "../utils/validationHandler"
import withLeadData from "./withLeadData"

const propTypes = {};

const defaultProps = {};

export default function withValidation(Component) {
    return withLeadData(class withValidation extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                showErrorBar: false
            };
            this.getRequiredFields = this.getRequiredFields.bind(this)
            this.validateField = this.validateField.bind(this)
            this.renderError = this.renderError.bind(this)
        }
        
        /**
         * VGuarda todos os elementos obrigatórios de um formulário.
         * @param {object} param0 - Objeto contendo name to atributo a ser validado
         */
        getRequiredFields({ name, component }) {
            if(!this.state[name] && !this.props.leadData[name] && component !== "input") {
                this.setState({ [name]: { hasError: false, erorrMsg: "", renderError: false }})
            }
            else if( !this.state[name] && !this.props.leadData[name] ) {
                this.setState({ [name]: { hasError: true, erorrMsg: "", renderError: false }})
            }
        }

        /**
         * Executa as funções de validação para cada atributo do input. O name passado como
         * parâmetro neste objeto precisa ser identico ao name da função de validação. Todas
         * as funções de validação estão no arquivo "./src/utils/validationHandler.js".
         * @param {object} leadData - Dados do lead.
         * @param {object} leadData.name  - Atributo name do input de onde o dado é inputado.
         * @param {object} leadData.value - Atributo value do input de onde o dado é inputado.
         */
        async validateField({ name, value }) {
            let result = await validator[name](value)
            const r = await result
            return await this.setState(currentState => ({ [name]: { ...currentState[name], ...r}, showErrorBar: r.showErrorBar }))
            
        }

        /**
         * Atualiza o estado do HOC para indicar se o componente com o name informado deve
         * renderizar o erro ou não.
         * @param {object} leadData - Objeto contendo os dados de um usuário
         * @param {object} leadData.name - Atributo name do input de onde o dado é inputado. 
         */
        renderError({ name }) {
            this.state[name].hasError &&
                this.setState(currentState => ({ [name]: { ...currentState[name], renderError: true } }))
            !this.state[name].hasError &&
                this.setState(currentState => ({ [name]: { ...currentState[name], renderError: false } }))
        }

        /**
         * Avalia o estado do HOC para definir se o botão deve ser desabilitado ou não. Avalia se
         * algum elemento do estado possui erro e se todos os campos do obrigatório possuem algum
         * dado inputado.
         */
        disableButton() {
            const { leadData } = this.props
            const hasError = Object.values(this.state).some( entry => entry.hasError ) // Retorna true se pelo menos 1 dos itens do estado tiver hasError === true
            const hasLeadData = Object.keys(this.state).every( element => {
                if( element !== "showErrorBar") {
                    return leadData[element] && leadData[element].length > 0 && leadData[element] !== "-1" // Retorna true se leadData tiver valor para todas as chaves do estado.
                }
                return true
            })
            return hasError || !hasLeadData
        }

        render() {
            return (
                <Component 
                    {...this.props} 
                    errors={this.state}
                    getRequiredFields={this.getRequiredFields}
                    validateField={this.validateField}
                    renderError={this.renderError}
                    disableButton={this.disableButton()}
                />
            )
        }
    })
}


 withValidation.propTypes = propTypes;
 withValidation.defaultProps = defaultProps;