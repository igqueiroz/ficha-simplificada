import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components"
import {ThemeConsumer} from "../contexts/ThemeProvider"
import {DataConsumer} from "../contexts/DataProvider"
import { StepConsumer } from "../contexts/StepProvider"

const propTypes = {};

const defaultProps = {};

const ies = process.env.REACT_APP_IES

require.context("../images/", true, /\-logo\.svg$/ )

/** Renderiza elemento no topo da página. Os textos não são dinâmicos, ou
 * seja, não mudam de IES para IES.
 */
export default function LogoBar(props) {


    return (
        <ThemeConsumer>
            {theme => (
                <DataConsumer>
                    {({ basicData }) => (
                        <Wrapper bkg={theme.topbar}>
                            <StepConsumer>
                                {({ step, totalSteps }) => {
                                    return step !== (totalSteps - 1) && <React.Fragment>
                                        <Text>Olá, seja bem-vindo {basicData.name}</Text>
                                        <Text>Preencha os campos abaixo e garanta sua vaga com o ENEM.</Text>
                                    </React.Fragment>
                                }}
                            </StepConsumer>
                        </Wrapper>
                    )}
                </DataConsumer>
            )}
        </ThemeConsumer>
    );
}

LogoBar.propTypes = propTypes;
LogoBar.defaultProps = defaultProps;

const Wrapper = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    flex-direction: column;
    background-color: ${ props => props.bkg }
    padding: 33px 0;
    justify-content: center;
    @media (max-width:600px) {
        padding: 24px 0 22px 0;
        ::before {
            position: absolute;
            content: '';
            background-color: ${ props => props.bkg };
            width: 100%;
            height: 94px;
            z-index: -1;
        }
    }
`

const Text = styled.p`
    color: #fff;
    margin: 0;
    padding: 0;

    @media (max-width:600px) {
        font-size: 11px;
    }

    @media (min-width: 601px) {
        font-size: 17px;
    }
`