import React from 'react';
import styled, { keyframes } from "styled-components"

export default function LoadingButton(props) {
    return (
        <Wrapper>
            <Animation>
                <Dot />
                <Dot />
                <Dot />
                <Dot />
            </Animation>
        </Wrapper>
    );
}

const animation1 = keyframes`
    0% {
        transform: scale(0);
    }
    100% {
        transform: scale(1);
    }
`

const animation2 = keyframes`
    0% {
        transform: translate(0, 0);
    }
    100% {
        transform: translate(19px, 0);
    }
`

const animation3 = keyframes`
    0% {
        transform: scale(1);
    }
    100% {
        transform: scale(0);
    }
`

const Wrapper = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
`

const Animation = styled.div`
    position: relative;
    width: 64px;
    bottom: 30px;
`

const Dot = styled.div`
    position: absolute;
    top: 27px;
    width: 11px;
    height: 11px;
    border-radius: 50%;
    background: #fff;
    animation-timing-function: cubic-bezier(0, 1, 1, 0);
    &:nth-child(1) {
        left: 6px;
        animation: ${animation1} 0.6s infinite;
    }
    &:nth-child(2) {
        left: 6px;
        animation: ${animation2} 0.6s infinite;
    }
    &:nth-child(3) {
        left: 26px;
        animation: ${animation2} 0.6s infinite;
    }
    &:nth-child(4) {
        left: 45px;
        animation: ${animation3} 0.6s infinite;
    }
`