import React from 'react';
import PropTypes from 'prop-types';
import styled, { keyframes } from "styled-components"

const propTypes = {};

const defaultProps = {};

export default function ProgressBarStep({ isSelected, isLastStep }) {
    return (
        <React.Fragment>    
            <ProgressStep isSelected={isSelected} />
            { !isLastStep && <Circle /> }
        </React.Fragment>
    );
}

ProgressBarStep.propTypes = propTypes;
ProgressBarStep.defaultProps = defaultProps;

const ProgressStep = styled.hr`
    flex: 1;
    border: ${ props => props.isSelected ? "3px" : "1px" } solid #707070;
`

const Circle = styled.div`
    width: 10px;
    height: 10px;
    border-radius: 100%;
    background-color: #707070;
`