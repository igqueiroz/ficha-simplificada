import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components"
import ProgressBar from "./ProgressBar"
import Step from "./Step"
import { StepConsumer } from "../contexts/StepProvider"

const propTypes = {};

const defaultProps = {};

export default function StepWrapper(props) {
    return (
        <Wrapper>
            <StepConsumer>
                {({ step, totalSteps }) => {
                    return step === (totalSteps - 1) && <div style={{textAlign: 'center', marginBottom: '30px'}}>
                        <strong>Parabéns! Você finalizou sua inscrição!</strong><br />
                        Confira os dados abaixo
                    </div>
                }}
            </StepConsumer>
            <ProgressBar />
            <Step token={props.token}>
                <Step.Step1 {...props} />
                <Step.Step2 {...props} />
                <Step.Step3 {...props} />
                <Step.Step4 {...props} />
            </Step>
        </Wrapper>
    );
}

StepWrapper.propTypes = propTypes;
StepWrapper.defaultProps = defaultProps;

const Wrapper = styled.main`
    width: 90%;
    max-width: 670px;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: auto;
    margin-top: 40px;
    @media (max-width:600px) {
        background: white;
        padding-bottom: 30px;
        width: calc(100% - 20px);
        margin: 0 10px;
        padding-top: 30px;
        box-shadow: 0 1px 6px 0 rgba(66, 79, 87, 0.16);
        border-radius: 20px;
        flex: 1;
    }
`