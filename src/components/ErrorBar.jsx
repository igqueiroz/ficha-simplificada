import React from 'react';
import PropTypes from 'prop-types';
import styled, { keyframes } from "styled-components"

const propTypes = {};

const defaultProps = {};

export default function ErrorBar({ text }) {
    return (
        <React.Fragment>
            <Wrapper>
                <Text>{ text }</Text>
            </Wrapper>
        </React.Fragment>
    );
}

ErrorBar.propTypes = propTypes;
ErrorBar.defaultProps = defaultProps;

const fadeIn = keyframes`
    from {
        opacity: 0;
    }
    to {
        opacity: 1;
    }
`

const Wrapper = styled.div`
    width: 100%;
    height: 40px;
    background-color: #fadada;
    border-radius: 20px;
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative;
    top: -175px;
    animation: ${fadeIn} .5s ease-in;
`

const Text = styled.p`
    font-size: 16px;
    color: #af2f2f;
`