import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components"
import withRefetch from "../hocs/withRefetch"
import Select from './Select';
import { isMobileOnly } from "react-device-detect"
import { ThemeConsumer } from "../contexts/ThemeProvider"
import RenderDrawer from "./RenderDrawer"
import Button from "./Button"

const propTypes = {};

const defaultProps = {};

/** 
 * Duas responsabilidades: define se o elemento renderizad será um select ou
 * um drawer de acordo com o dispositivo cliente, e armazena o estado do drawer
 * para avaliar se deve ser aberto ou não.
 */
class Drawer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpen: false
        }
        this.toggleDrawer = this.toggleDrawer.bind(this)
    }

    toggleDrawer() {
        this.setState( currentState => ({ isOpen: !currentState.isOpen }))
    }

    renderComponent(render, theme) {
        switch(render) {
            case "select":
                return isMobileOnly
                    ? <Select {...this.props} />
                    : <FakeSelect 
                        {...this.props} 
                        onClick={this.toggleDrawer} 
                        isOpen={this.state.isOpen} 
                        theme={theme}
                    >
                            <Placeholder>{ this.props.leadData[this.props.alias] || this.props.placeholder }</Placeholder>
                    </FakeSelect>
            case "button":
                return (
                    <Button>
                        <Button.Desconto 
                            theme={theme} 
                            text={this.props.text} 
                            marginTop="0" 
                            actions={[
                                "toggleDrawer", 
                                this.toggleDrawer, 
                                "dataLayerEvent",
                                this.props.updateLeadData
                            ]}  
                        />
                    </Button>
                )
            default:
                throw new Error("Componente para renderizar o toggle do drawer não encontrado.")
        }
    }

    render() {
        return (
            <ThemeConsumer>
                {theme => (
                    <React.Fragment>
                        {this.renderComponent(this.props.render, theme)}
                        <RenderDrawer 
                            {...this.props}
                            isOpen={this.state.isOpen}
                            theme={theme} 
                            background={ theme[this.props.name+"Bkg"] }
                            toggleDrawer={this.toggleDrawer}
                        />
                    </React.Fragment>
                )}
            </ThemeConsumer>
        )
    }
}

export default withRefetch(Drawer)

Drawer.propTypes = propTypes;
Drawer.defaultProps = defaultProps;

const FakeSelect = styled.div`
    border-bottom:  1px solid ${props => props.isOpen ? props.theme.inputFocus : "#2b2d5c" };
    opacity: 0.6;
    cursor: pointer;
    display: flex;
    align-items: end;

    @media (min-width:601px) {
        width:${ props => props.width ? props.width : "100%" };
    }
    @media (max-width:600px) {
        width: 100%;
    }
`

const Placeholder = styled.p`
    width: 100%;
    font-size: 16px;
    padding-bottom: 10px;
    margin: 40px 0 0 0;
    transition: .3s;
    color: #2b2d5c;
`

const FakeButton = styled.div`

`