import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components"
import { StepConsumer } from "../contexts/StepProvider"
import ProgressBarStep from './ProgressBarStep';


const propTypes = {};

const defaultProps = {};

export default function ProgressBar(props) {
    function renderProgressSteps(totalSteps, currentStep) {
        let components = []
        for( let i = 0; i < totalSteps; i++ ) {
            components.push(ProgressBarStep)
        }
        return components.map((_, i) => (
            <ProgressBarStep key={i} isSelected={i === currentStep} isLastStep={i === totalSteps-1} />
        ))
    }

    return (
        <StepConsumer>
            {({ totalSteps, step }) => (
                <Wrapper>
                    {renderProgressSteps(totalSteps, step)}
                </Wrapper>
            )}
        </StepConsumer>
    );
}

ProgressBar.propTypes = propTypes;
ProgressBar.defaultProps = defaultProps;

const Wrapper = styled.div`
    width: 90%;
    max-width: 670px;
    display: flex;
    flex-wrap: nowrap;
    align-items: center;
    height: 10px;
    @media (min-width:601px) {
        margin-bottom: 20px;
    }
`