import React from 'react';
import PropTypes from 'prop-types';
import { StepConsumer } from "../contexts/StepProvider"
import RenderForm from "./RenderForm"
import Conclusao from "./Conclusao"

const propTypes = {};

const defaultProps = {};

/** 
 * Componente que define em qual o step o usuário está no momento. Para isso ele faz
 * um loop entre os Steps declarados no componente Root, dentro do método Root.
 * */
class Steps extends React.Component {
    static Step1 = props => <RenderForm {...props} />
    static Step2 = props => <RenderForm {...props} />
    static Step3 = props => process.env.REACT_APP_IES === "fmu" ? <RenderForm {...props} /> : <Conclusao {...props} />
    static Step4 = props => <Conclusao {...props} />

    render() {
        return (
            <React.Fragment>
                <StepConsumer>
                    {({ step, avancar, voltar }) => {
                        return React.Children.map(this.props.children, (element, i) => {
                            if( step === i ) {
                                return React.cloneElement(element, { 
                                    step,
                                    avancar,
                                    voltar
                                })
                            }
                            return null
                        })
                    }}
                </StepConsumer>
            </React.Fragment>
        );
    }
}

export default Steps

Steps.propTypes = propTypes;
Steps.defaultProps = defaultProps;