import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components"
import withTheme from "../hocs/withTheme"
import { LeadDataConsumer } from "../contexts/LeadDataProvider"
import MaskedInput from "react-text-mask"
import ErrorMsg from "./ErrorMsg"

const propTypes = {
    /** Placeholder do input. */
    placeholder: PropTypes.string.isRequired,
    /** Atributo name da tag input */
    name: PropTypes.string.isRequired,
    /** Atributo ID da tag input */
    id: PropTypes.string,
    /** Largura para o input */
    width: PropTypes.string,
    /** Objeto contendo o esquema de cores para a IES ativa. */
    theme: PropTypes.shape({
        inputFocus: PropTypes.string.isRequiredm,
        text: PropTypes.string.isRequired
    })

};

const defaultProps = {};

/** Descrição da UI de um componente input. */
function Input(props) {
    function handleChange(data, updateLeadData) {
        props.required && props.validateField(data)
        updateLeadData(data)
    }
    
    return (
        <LeadDataConsumer>
            {({ updateLeadData, leadData }) => (
                <InputField width={props.width}>
                    <MaskedInput 
                        {...props} 
                        type="text"
                        onChange={event => handleChange(event.target, updateLeadData)}
                        onBlur={event => props.required && props.renderError(event.target)}
                        value={leadData[props.name]}
                        render={(ref, props) => <StyledInput ref={ref} {...props} />}
                    />
                    { props.error && props.error.hasError && props.error.renderError && <ErrorMsg /> }
                </InputField>
            )}
        </LeadDataConsumer>
    );
}

export default withTheme(Input)

Input.propTypes = propTypes;
Input.defaultProps = defaultProps;

const InputField = styled.div`
    @media (min-width:601px) {
        width:${ props => props.width ? props.width : "100%" };
    }
    @media (max-width:600px) {
        width: 100%;
    }
`

const StyledInput = styled.input`
    width: 100%;
    border: none;
    outline: none;
    border-bottom: 1px solid #2b2d5c;
    font-size: 16px;
    padding-bottom: 10px;
    margin-top: 40px;
    transition: .3s;
    opacity: 0.6;
    color: #2b2d5c;

    &:focus {
        border-bottom: 1px solid ${props => props.theme.inputFocus}
        color: ${ props => props.theme.text }
        transition: .3s;
        opacity: 1;

        &::placeholder {
            color: ${ props => props.theme.text };
            opacity: 1;
            transition: color .3s;
        }
    }

    &::placeholder{
        opacity: 0.6;
        color: #2b2d5c;
        transition: .3s;
    }
`