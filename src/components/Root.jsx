import React from 'react';
import PropTypes from 'prop-types';
import Logobar from "./Logobar"
import Topbar from "./Topbar"
import StepWrapper from "./StepWrapper"
import { isMobileOnly } from "react-device-detect"
import { Helmet } from "react-helmet"
import { DataConsumer } from "../contexts/DataProvider" 
import withAbandono from "../hocs/withAbandono"
import http from "../utils/requestHandler"

class Root extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    componentDidMount() {
        http.get( "/token" ).then( res => this.setState({ token: res.data.auth }) )
    }

    componentWillUnmount() {
        document.removeEventListener("DOMContentLoaded", this.scrollToTop )
    }

    render() {
        return (
            <React.Fragment>
                <DataConsumer>
                    {(({ basicData }) => (
                        <Helmet>
                            <meta charSet="utf-8" />
                            <title>Bem-vindo {basicData.name}</title>
                            <link rel="shortcut icon" href={ require(`../images/${process.env.REACT_APP_IES}-favicon.png`) } />
                            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
                        </Helmet>
                    ))}
                </DataConsumer>
                
                { !isMobileOnly && <Logobar logo={`${process.env.REACT_APP_IES}-logo.svg`} /> }
                <Topbar />
                <StepWrapper token={this.state.token} />
                { isMobileOnly && <Logobar logo={`${process.env.REACT_APP_IES}-logo.svg`} /> }
            </React.Fragment>
        );   
    }
}

export default withAbandono(Root)