import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components"
import Form from "./Form"
import Select from "./Select"
import Button from "./Button"
import { ThemeConsumer } from "../contexts/ThemeProvider"
import { DataConsumer } from "../contexts/DataProvider"
import { LeadDataConsumer } from "../contexts/LeadDataProvider"
import { StepConsumer } from "../contexts/StepProvider"
import http from "../utils/requestHandler"
import { dataLayerEvent } from "../utils/gtm"

const propTypes = {};

const defaultProps = {};

export default function FormAgendamento(props) {
    function genDates(numberOfDays) {
        const todayFullDate = new Date()
        const dates = []
        for(let i = 1; i <= numberOfDays; i++) {
            const dayPlusOne = new Date( new Date().setDate(todayFullDate.getDate() + i))
            if( dayPlusOne.getDay() !== 0 ) {
                const date = dayPlusOne.toLocaleString("pt-BR").split(' ')[0]
                if( process.env.REACT_APP_IES !== "unp" && process.env.REACT_APP_IES !== "fpb" ) {
                    dates.push({ value: date, name: date })
                }
                else if( dayPlusOne.getDate() <= 25 ) {
                    dates.push({ value: date, name: date })
                }
            }
        }
        return dates
    }
    return (
            <ThemeConsumer>
                {theme => (
                    <React.Fragment>
                        <Text color={theme.agendamentoText}>Agende agora sua matrícula e garanta sua vaga. É rapidinho!</Text>
                        <LeadDataConsumer>
                        {({ leadData, updateLeadData }) => (
                            <DataConsumer>
                                {({ conclusao }) => (
                                    <StepConsumer>
                                        {({ step }) => (
                                            <Form>
                                                <Select
                                                    name="agendamentoData"
                                                    borderBottom="#fff"
                                                    color="#fff"
                                                    opacity="1"
                                                    placeholder="Data"
                                                    color={theme.agendamentoText}
                                                    options={genDates(4)}
                                                />
                                                <Select
                                                    name="agendamentoCampus"
                                                    borderBottom="#fff"
                                                    color={theme.agendamentoText}
                                                    opacity="1"
                                                    placeholder={ process.env.REACT_APP_IES === "unp" ? "Unidade" : "Campus"}
                                                    options={conclusao.campus}
                                                />
                                                <Button>
                                                    <Button.Agendamento 
                                                        text="AGENDAR" 
                                                        actions={[
                                                            "putData", 
                                                            () => http.put("/inscricao", { ...leadData, evento: "agendamento" })
                                                                    .then( _ => {
                                                                            updateLeadData({ name: "agendado", value: true })
                                                                            dataLayerEvent(step+2)
                                                                        })
                                                        ]} 
                                                    />
                                                    </Button>
                                            </Form>
                                        )}
                                    </StepConsumer>
                                )}
                            </DataConsumer>
                        )}
                        </LeadDataConsumer>
                    </React.Fragment>
                )}
            </ThemeConsumer>
        
    );
}

FormAgendamento.propTypes = propTypes;
FormAgendamento.defaultProps = defaultProps;

const Text = styled.h1`
    font-size: 18px;
    width: 100%;
    text-align: center;
    color: ${ props => props.color }
`