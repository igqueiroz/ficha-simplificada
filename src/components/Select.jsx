import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components"
import withRefetch from "../hocs/withRefetch"
import _ from "lodash"

const propTypes = {
    /** Atributo name da tag select. */
    name: PropTypes.string.isRequired,
    id: PropTypes.string,
    width: PropTypes.string,
    updateLeadData: PropTypes.func,
    options: PropTypes.array,
    data: PropTypes.array,
    placeholder: PropTypes.string,
    alias: PropTypes.string,
    borderBottom: PropTypes.string,
    color: PropTypes.string,
    opacity: PropTypes.string
};

const defaultProps = {};

/** 
 * Renderiza um select com suas options.
 */
class Select extends React.Component {
    renderOptions() {
        const { data, options } = this.props
        const uniqueData = _.uniqBy(this.props.data, 'value');
        const opt = options || uniqueData
        return opt.length > 0
        ? opt.map( option => {
            return (
                <option 
                    key={option.value} 
                    value={option.value}
                >
                    {option.name.charAt(0).toUpperCase() + option.name.slice(1)}
                </option>
            )
        })
        : <option>Carregando...</option>
    }

    handleChange(event) {
        const { alias, updateLeadData } = this.props
        updateLeadData({ name: event.target.name, value: event.target.value })
        const index = event.nativeEvent.target.selectedIndex;
        alias && updateLeadData({ name: alias, value: event.nativeEvent.target[index].text })
    }

    render() {
        const { name, id, borderBottom, color, opacity, placeholder, width, leadData } = this.props
        return (
            <InputField width={width}>
                <StyledSelect 
                    name={name} 
                    id={id}
                    borderBottom={borderBottom}
                    color={color}
                    opacity={opacity}
                    value={leadData[name]} 
                    onChange={ event => this.handleChange(event)}
                >
                    <option value="-1" selected disabled >{ placeholder }</option>
                    { this.renderOptions() }
                </StyledSelect>
            </InputField>
        );
    }
    
}

export default withRefetch(Select)

 Select.propTypes = propTypes;
 Select.defaultProps = defaultProps;

const InputField = styled.div`
    display: flex;
    align-items: end;
    @media (min-width:601px) {
        width:${ props => props.width ? props.width : "100%" };
    }
    @media (max-width:600px) {
        width: 100%;
    }
`

const StyledSelect = styled.select`
    background: white;
    width: 100%;
    border: none;
    outline: none;
    border-bottom: 1px solid ${ props => props.borderBottom ? props.borderBottom : "#2b2d5c" };
    font-size: 16px;
    padding-bottom: 10px;
    margin-top: 40px;
    transition: .3s;
    appearance: none;
    opacity: ${ props => props.opacity ? props.opacity : "0.6" };
    color: ${ props => props.color ? props.color : "#2b2d5c" };
    background: transparent;

    &:focus {
        border-bottom: 1px solid ${props => props.theme.inputFocus}
        color: ${ props => props.theme.text }
        transition: .3s;
        opacity: 1;
    }

    option {
        color: black;
    }
}
`