import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components"
import { LeadDataConsumer } from "../contexts/LeadDataProvider"

const propTypes = {};

const defaultProps = {};

export default function Radio(props) {
    function renderRadios(campi) {
        return campi.map(campus => {
            return (
                <LeadDataConsumer>
                    {({ leadData, updateLeadData }) => (
                        <Label>
                            <HiddenInput 
                                type="radio" 
                                name={props.name} 
                                value={campus.value} 
                                checked={campus.value == leadData[props.name]}
                                onChange={() => handleChange(campus, updateLeadData)}
                            />
                            <Circle>
                                {campus.value == leadData[props.name] && <InnerCircle />}
                            </Circle>
                            { campus.name }
                        </Label>
                    )}
                </LeadDataConsumer>
            )
        })
    }

    function handleChange(campus, action) {
        action({ name: props.name, value: campus.value })
        action({ name: props.alias, value: campus.name })
        props.toggleDrawer()
    }

    return (
        <React.Fragment>
            {renderRadios(props.data)}
        </React.Fragment>
    );
}

Radio.propTypes = propTypes;
Radio.defaultProps = defaultProps;

const Label = styled.label`
    color: #fff;
    font-size: 17px;
    text-transform: uppercase;
    cursor: pointer;
    width: 100%;
    display: flex;
    margin-top: 70px;

    &:first-of-type {
        margin-top: 12px;
    }
`

const HiddenInput = styled.input`
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
`

const Circle = styled.div`
    width: 16px;
    height: 16px;
    border-radius: 100%;
    border: 1px solid #fff;
    margin-right: 35px;
    display: flex;
    align-items: center;
    justify-content: center;
`

const InnerCircle = styled.div`
    width: 10px;
    height: 10px;
    background-color: #fff;
    border-radius: 100%;
`