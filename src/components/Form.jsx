import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components"
import { LeadDataConsumer } from "../contexts/LeadDataProvider"
import withValidation from "../hocs/withValidation"
import withAbandono from "../hocs/withAbandono"
import ErrorBar from "./ErrorBar"

const propTypes = {};

const defaultProps = {};

class Form extends React.Component {
    constructor(props) {
        super(props)
        this.onUnload = this.onUnload.bind(this)
    }

    onUnload() {
        const token = ie => {
            switch( ie ) {
                case 'unn':
                    return "5ba993af4cc85dd37667d8b91e94f184";
                case 'ufg':
                    return "d1be35ae84c69fac1941a1d64214a5ab";
                case 'fpb':
                    return "7dbf6ad00f4fe59eabb48398ea25ffc9";
                case 'unp':
                    return "635dfc1c5acd2b88196b6ba21e1d8a75";
                case 'unf':
                    return "a2f4118de52f0d063fc8a9f00a1b8a0b";
                case 'uam':
                    return "04d713eec86c06caa067bfcaf866499d";
                case 'fmu':
                    return "3470254e3b62770353646b8c97e67a65";
                case 'ibm':
                    return "9d8769814b7242dfedb5b1b4037fd78d";
                case 'fad':
                    return "49e6adfa85df20dc3fd43bc796c904df";
                case 'unr':
                    return "975cdf028ac20021ab6d845d73847ef8";
                case 'CE':
                    return '635dfc1c5acd2b88196b6ba21e1d8a75';
                default:
                    return "9b77707c7865bf2c4b2c0c054d895314";
            };
        }
        const beaconData = {
            token_rdstation: token(process.env.REACT_APP_IES),
            identificador: "ficha-wr-enem-abandono",
            evento: "abandono",
        }
        const fullData = { ...this.props.leadData , ...beaconData  }
        const headers = {
            type: "application/json"
        }
        var client = new XMLHttpRequest();
        if( this.props.step.step < this.props.step.totalSteps-1 ) {
            client.open("POST", "https://www.rdstation.com.br/api/1.3/conversions", false); // third parameter indicates synchronous ajax
            client.setRequestHeader("Content-Type", "application/json");
            client.send(JSON.stringify(fullData));
        }
    }

    componentDidMount() {
        if( process.env.NODE_ENV !== "development" ) {
            window.addEventListener("beforeunload", this.onUnload)
        }
    }
 
    componentWillUnmount() {
        if( process.env.NODE_ENV !== "development" ) {
            window.removeEventListener("beforeunload", this.onUnload)
        }
    }
    render() {
        return (
            <LeadDataConsumer>
                {({ leadData, updateLeadData }) => (
                    <StyledForm>
                        {React.Children.map( this.props.children, element => {
                            element.props.required && this.props.getRequiredFields(element.props)
                            return React.cloneElement(element, {
                                error: this.props.errors[element.props.name] || null,
                                validateField: this.props.validateField,
                                renderError: this.props.renderError,
                                disableButton: this.props.disableButton
                            })
                        })}
                        { this.props.errors.showErrorBar && <ErrorBar text="CPF já inscrito. Em caso de dúvida entre em contato com a central de atendimento." /> }
                    </StyledForm>
                )}
            </LeadDataConsumer>
        );
    }
}

export default withAbandono(withValidation(Form))

Form.propTypes = propTypes;
Form.defaultProps = defaultProps;

const StyledForm = styled.form`
    width: 90%;
    max-width: 670px;
    margin: auto;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    @media (max-width:600px) {
        width: 100%;
        padding: 0 25px;
        box-sizing: border-box;
        flex: 1;
        align-content: flex-start;
    }
`