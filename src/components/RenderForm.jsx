import React from 'react';
import PropTypes from 'prop-types';
import { DataConsumer } from "../contexts/DataProvider"
import Form from "./Form"
import Input from "./Input"
import Select from "./Select"
import Drawer from "./Drawer"
import Button from "./Button"
import withAbandono from '../hocs/withAbandono';
import withLeadData from '../hocs/withLeadData';
import axios from "axios"

const propTypes = {
    /** Step atual com o index do arquivo modelo para fazer o loop. */
    step: PropTypes.number.isRequired
};

const defaultProps = {};

/** Le um arquivo modelo com a descrição do Form e renderiza o Form em tela. */
class RenderForm extends React.Component {
    renderBody(form) {
        return form[this.props.step.step].map( element => {
            switch(element.component) {
                case "input":
                    return <Input key={element.id} {...element} {...this.props} />
                case "select":
                    return <Select key={element.id} {...element} {...this.props} />
                case "drawer":
                    return <Drawer key={element.id} {...element} {...this.props} />
                case "buttonInicial":
                    return <Button><Button.Inicial key={element.id} {...element} {...this.props} /></Button>
                case "buttonProximo":
                    return <Button><Button.Proximo key={element.id} {...element} {...this.props} /></Button>
                case "buttonVoltar":
                    return <Button><Button.Voltar key={element.id} {...element} {...this.props} /></Button>
                case "buttonDesconto":
                    return <Button><Button.Desconto key={element.id} {...element} {...this.props} /></Button>
                case "buttonAgendamento":
                    return <Button><Button.Agendamento key={element.id} {...element} {...this.props} /></Button>
                default:
                    throw new Error("Erro na função render do componente RenderForm.")
            }
        })
    }

    render() {
        return (
            <DataConsumer>
                {({ form }) => (
                    <Form>
                        {this.renderBody(form)}
                    </Form>
                )}
            </DataConsumer>
        );
    }
    
}

export default withAbandono(RenderForm)

RenderForm.propTypes = propTypes;
RenderForm.defaultProps = defaultProps;