import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components"
import SVGArrow from "./SVGArrow"
import LoadingButton from "./LoadingButton"
import { ThemeConsumer } from "../contexts/ThemeProvider"
import { StepConsumer } from "../contexts/StepProvider"
import { SendDataConsumer } from "../contexts/SendDataProvider"
import { LeadDataConsumer } from "../contexts/LeadDataProvider"
import { isMobileOnly } from "react-device-detect"
import { dataLayerEvent } from "../utils/gtm"
import { isIOS } from "react-device-detect"

const propTypes = {
    /** Texto do botão. */
    text: PropTypes.string
};

const defaultProps = {};

class Button extends React.Component {
    static Inicial = props => (
        <StyledButton {...props} textColor={ props.theme.buttonInicialText } >
            {props.text} { !isIOS && <SVGArrow fill={props.theme.svgIcon} /> }
        </StyledButton>
    )

    static Proximo = props => (
        <StyledButton {...props} textColor="#fff" background={ props.theme.buttonProximoBkg } >
            {props.isLoading ? <LoadingButton /> : props.text}
        </StyledButton>
    )

    static Desconto = props => (
        <StyledButton {...props} widthMobile="90%" width="80%" borderRadius="12px" fontSize="20px" marginTop="25px" textColor="#fff" background={ props.theme.buttonDescontoBkg } >
            {props.text}
        </StyledButton>
    )

    static Agendamento = props => (
        <StyledButton {...props} marginTop="55px" textColor="#fff" background={ props.theme.buttonAgendarBkg } disabled={false} >
            {props.isLoading ? <LoadingButton /> : props.text}
        </StyledButton>
    )

    static Voltar = props => (
        <StyledButton {...props} textColor={ props.theme.buttonInicialText } >
            { isMobileOnly ? isIOS && <SVGArrow fill={props.theme.svgIcon} direcao="esquerda" position="relative" /> : props.text } 
        </StyledButton>
    )

    handleClick(event, condition, action) {
        event.preventDefault()
        condition && action.map( a => a.action(a.params, a.callback, a.condition))
    }

    render() {
        return (
            <ThemeConsumer>
                {theme => (
                    <StepConsumer>
                        {({ avancar, voltar, step }) => (
                            <SendDataConsumer>
                            {({ isLoading, postData }) => (
                                <LeadDataConsumer>
                                    {({ leadData, updateLeadData }) => (
                                        React.Children.map( this.props.children, element => {
                                            const action = []
                                            // gambi alert. Cadastro cada action dos botões em um array.
                                            element.props.actions && element.props.actions.includes("avancar") && action.push({ action: avancar })
                                            element.props.actions && element.props.actions.includes("voltar") && action.push({ action: voltar })
                                            element.props.actions && element.props.actions.includes("postData") && action.push({ action: postData, params: leadData, callback: avancar })
                                            element.props.actions && element.props.actions.includes("toggleDrawer") && action.push({ action: element.props.actions[1] })
                                            element.props.actions && element.props.actions.includes("href") && action.push({ action: element.props.actions[1] })
                                            element.props.actions && element.props.actions.includes("putData") && action.push({ action: element.props.actions[1], callback: element.props.actions[2], callbackParam: { agendado: true } })
                                            element.props.actions && element.props.actions.includes("dataLayerEvent") && action.push({ action: () => dataLayerEvent(step+1, element.props.actions[3], true)})
                                            return React.cloneElement(element, {
                                                ...this.props,
                                                theme,
                                                isLoading,
                                                disabled: element.props.shouldBlock ? this.props.disableButton : false,
                                                onClick: event => this.handleClick(event, !isLoading, action)
                                            })
                                        })
                                    )}
                                </LeadDataConsumer>
                            )}
                            </SendDataConsumer>
                        )}
                    </StepConsumer>
                )}
            </ThemeConsumer>
        )
    }
}

export default Button

Button.propTypes = propTypes;
Button.defaultProps = defaultProps;

const StyledButton = styled.button`
    max-width: 410px;
    padding: 20px 0;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: ${ props => props.borderRadius ? props.borderRadius : "27px" };
    border: none;
    outline: none;
    color: ${ props =>  props.textColor };
    font-size: ${ props => props.fontSize ? props.fontSize : "16px" };
    box-shadow: ${ props => props.disabled ? "0 2px 12px 0 rgba(0, 0, 0, 0.16)" : "0 5px 15px 0 rgba(0, 0, 0, 0.16)" };
    cursor: ${ props => props.disabled ? "not-allowed" : "pointer" };
    transition: .3s;
    position: relative;
    background-color: ${ props => props.background ? props.background : "#fff" };
    margin: auto;
    margin-bottom: 40px;
    opacity: ${ props => props.disabled ? "0.6" : props.isLoading ? "0.6" : "1" };

    &:hover {
        box-shadow: 0 2px 12px 0 rgba(0, 0, 0, 0.16);
        transition: .3s;
    }

    @media (max-width: 600px) {
        margin-top: ${ props => props.marginTop ? props.marginTop : "55px" };
        width: ${ props => props.widthMobile ? props.widthMobile : "205px" };
    }

    @media (min-width: 601px) {
        margin-top: ${ props => props.marginTop ? props.marginTop : "95px" };
        width: ${ props => props.width ? props.width : "225px" };
    }
`

const Icon = styled.img`

`