import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components"

const propTypes = {};

const defaultProps = {};

export default function ErrorMsg(props) {
    return <Text>Ops! Preenchimento incorreto!</Text>
}

ErrorMsg.propTypes = propTypes;
ErrorMsg.defaultProps = defaultProps;

const Text = styled.span`
    font-size: 14px;
    color: #f70d0d;
`