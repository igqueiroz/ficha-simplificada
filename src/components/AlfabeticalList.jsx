import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components"
import _ from "lodash"

const propTypes = {};

const defaultProps = {};

/**
 * Renderiza lista de cursos com separação por ordem alfabética. Como está sendo usado
 * apenas em uma renderização no app, está fortemente acoplado às ações do curso.
 */
export default function AlfabeticalList(props) {
    const groupedList = Object.entries(_.groupBy( props.data, entry => entry.name[0] ))

    function renderGroups(data) {
        return data.map( courseGroup => {
            const char = courseGroup[0]
            const courses = courseGroup[1]
            return(
                <React.Fragment key={char}>
                    <GroupChar>{ char }</GroupChar>
                    <GroupList>
                        {renderCourses( courses )}
                    </GroupList>
                </React.Fragment>
            )
        })
    }

    function renderCourses(courses) {
        return courses.map( course => {
            return(
                <Course 
                    key={course.value}
                    onClick={ () => handleClick(course, props.updateLeadData) }
                    isSelected={ props.leadData.valueCurso === course.value }
                >
                    { course.name }
                </Course>
            )
        })
    }

    function handleClick(course, action) {
        action({ name: props.name, value: course.value })
        action({ name: props.alias, value: course.name })
        props.toggleDrawer()
    }

    return (
        <React.Fragment>
            {renderGroups(groupedList)}
        </React.Fragment>
    );
}

AlfabeticalList.propTypes = propTypes;
AlfabeticalList.defaultProps = defaultProps;

const GroupChar = styled.p`
    width: 100%;
    font-size: 20px;
    color: #797979;
    font-weight: bold;
`

const GroupList = styled.ul`
    width: 100%;
    padding: 0;
    margin: 0;
    list-style: none;
    border-left: 1px solid #707070;
    margin-left: 7px;
    margin-bottom: 40px;
`

const Course = styled.li`
    padding-left: 60px;
    font-size: 17px;
    color: ${ props => props.isSelected ? "#222220" : "#797979" };
    font-weight: ${ props => props.isSelected ? "bold" : "regular" };
    margin-bottom: 47px;
    cursor: pointer;


    &:last-of-type {
        margin-bottom: 0;
    }
`