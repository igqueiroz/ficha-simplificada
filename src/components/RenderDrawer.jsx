import React from 'react';
import PropTypes from 'prop-types';
import styled, { keyframes } from "styled-components"
import { isMobileOnly } from "react-device-detect"
import CloseDrawer from "./CloseDrawer"
import RenderDrawerContent from "./RenderDrawerContent"

const propTypes = {};

const defaultProps = {};

export default class RenderDrawer extends React.Component {
    constructor(props) {
        super(props)
        this.state = { 
            isOpen: false,
            query: ""
        }
    }

    componentDidMount() {
        this.setState({ isOpen: this.props.isOpen})
    }

    componentDidUpdate(prevProps) {
        prevProps.isOpen !== this.props.isOpen && this.setState({ isOpen: this.props.isOpen })
    }

    render() {
        return (
            <Wrapper background={this.props.background} isOpen={this.state.isOpen}>
                <InnerWrapper>
                    <CloseDrawer onClick={this.props.toggleDrawer} />
                    <Margin>
                        <RenderDrawerContent {...this.props} />
                    </Margin>
                </InnerWrapper>
            </Wrapper>
        );
    }
}

RenderDrawer.propTypes = propTypes;
RenderDrawer.defaultProps = defaultProps;

const slideIn = keyframes`
    0% {
        right: ${ isMobileOnly ? "-80vw" : "-595px" };
    }

    100% {
        right: 0;
    }
`

const slideOut = keyframes`
    0% {
        right: 0;
    }
    100% {
        right: ${ isMobileOnly ? "-80px" : "-595px" };
    }
`

const Wrapper = styled.div`
    width: 90vw;
    max-width: 565px;
    background-color: ${ props => props.background };
    position: fixed;
    right: ${ props => props.isOpen ? "0" : "-595px" };
    bottom: 0;
    opacity: 1;
    z-index: 1;
    box-shadow: -6px 3px 6px 0 rgba(181, 181, 181, 0.27);
    border-top-left-radius: 10px;
    border-bottom-left-radius: 10px;
    animation: ${ props => props.isOpen ? slideIn : slideOut } .4s ease-in-out;
    overflow-y: auto;

    @media (max-width:600px) {
        height: 100vh;
    }

    @media (min-width:601px) {
        top: 0;
        position: fixed;
    }
`

const InnerWrapper = styled.div`
    width: 90%;
    margin: auto;
    display: flex;
    flex-wrap: nowrap;

    @media (max-width:600px) {
        margin-top: 30px;
    }

    @media (min-width:601px) {
        margin-top: 40px;
    }
`

const Margin = styled.div`
    width: 100%;
    @media (max-width:600px) {
        margin-left: 20px;
    }
    @media (min-width:601px) {
        margin-left: 80px;
    }
`