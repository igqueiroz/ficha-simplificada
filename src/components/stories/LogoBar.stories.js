import React from "react"
import { wInfo } from "./__withInfoStyle"
import { storiesOf } from "@storybook/react"
import Logobar from "../Logobar"

storiesOf("Logo", module)
    .addWithJSX("Unifacs", wInfo()(() => {
        return <Logobar logo={`unf-logo.svg`} />
    }))
    .addWithJSX("EAD Laureate", wInfo()(() => {
        return <Logobar logo={`ead-wide-logo.svg`} />
    }))
    .addWithJSX("Fadergs", wInfo()(() => {
        return <Logobar logo={`fad-logo.svg`} />
    }))
    .addWithJSX("FMU", wInfo()(() => {
        return <Logobar logo={`fmu-logo.svg`} />
    }))
    .addWithJSX("FPB", wInfo()(() => {
        return <Logobar logo={`fpb-logo.svg`} />
    }))
    .addWithJSX("ibm", wInfo()(() => {
        return <Logobar logo={`ibm-logo.svg`} />
    }))
    .addWithJSX("Anhembi", wInfo()(() => {
        return <Logobar logo={`uam-logo.svg`} />
    }))
    .addWithJSX("ufg", wInfo()(() => {
        return <Logobar logo={`ufg-logo.svg`} />
    }))
    .addWithJSX("Uninorte", wInfo()(() => {
        return <Logobar logo={`unn-logo.svg`} />
    }))
    .addWithJSX("UnP", wInfo()(() => {
        return <Logobar logo={`unp-logo.svg`} />
    }))
    .addWithJSX("UniRitter", wInfo()(() => {
        return <Logobar logo={`unr-logo.svg`} />
    }))