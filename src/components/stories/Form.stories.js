import React from "react"
import { wInfo } from "./__withInfoStyle"
import { storiesOf } from "@storybook/react"
import centered from '@storybook/addon-centered';
import Input from "../Input"
import Button from "../Button"
import colors from "../../models/colors"
import { text } from "@storybook/addon-knobs/react"

storiesOf("Form", module)
    .addDecorator(centered)
    .addWithJSX("Input", wInfo()(() => {
        return (
            <Input placeholder="Modelo de input" width="670px" theme={colors(text("IES", "unf"))} />
        )
    }))
    .addWithJSX("Botão", wInfo()(() => {
        return (
            <Button background="#fff" color="#fff">Próximo</Button>
        )
    }))