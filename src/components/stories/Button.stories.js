import React from "react"
import { wInfo } from "./__withInfoStyle"
import { storiesOf } from "@storybook/react"
import centered from '@storybook/addon-centered';
import Button from "../Button"
import colors from "../../models/colors"
import { text } from "@storybook/addon-knobs"

storiesOf("Botões", module)
    .addDecorator(centered)
    .addWithJSX("Unifacs 1", wInfo()(() => {
        const theme = colors(text( "ies", "unf" ))
        return (
            <Button>
                <Button.Inicial text="Próximo" textColor="" />
            </Button>
            //<Button background="#fff" textColor="#d10a11" text="Próximo" fill="#d10a11" icon />
        )
    }))