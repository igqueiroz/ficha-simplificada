import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components"
import { DataConsumer } from "../contexts/DataProvider"
import { ThemeConsumer } from "../contexts/ThemeProvider"

const propTypes = {};

const defaultProps = {};

export default function Descontos(props) {
    function renderCards(descontos) {
        return descontos.map(  desconto => {
            return (
                <ThemeConsumer>
                    {theme => (
                        <Card>
                            <Oferta color={theme.text}>{desconto.oferta}</Oferta>
                            <Range color={theme.text}>{ desconto.range }</Range>
                        </Card>
                    )}
                </ThemeConsumer>
            )
        })
    }
    return (
        <React.Fragment>
            <DataConsumer>
                {({ descontos }) => (
                    <ThemeConsumer>
                        {theme => (
                            <Wrapper>
                                {process.env.REACT_APP_IES !== "ufg" &&
                                <Text color={theme.agendamentoText}>Só com a nota do ENEM você ganha mais! Veja quanto vale a sua bolsa de acordo com a sua pontuação.</Text>}
                               {renderCards(descontos)}
                            </Wrapper>
                        )}
                    </ThemeConsumer>
                )}
            </DataConsumer>
        </React.Fragment>
    );
}

Descontos.propTypes = propTypes;
Descontos.defaultProps = defaultProps;

const Wrapper = styled.div`
    width: 90%;
    display: flex;
    flex-wrap: wrap;
    margin: auto
`

const Text = styled.p`
    width: 100%;
    color: ${ props => props.color };
    font-size: 18px;
    text-align: center;
    margin-bottom: 30px;
`

const Card = styled.div`
    background-color: #fff;
    border-radius: 10px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding: 6px 0;
    margin-bottom: 17px;

    @media (max-width: 600px) {
        width: 100%;
    }
    @media (min-width:601px) {
        width: 47.5%;
        margin-right: 5%;
    }

    &:nth-child(odd) {
        margin-right: 0;
    }
`

const Oferta = styled.p`
    color: ${ props => props.color };
    font-size: 16px;
    margin: 0;
    font-weight: bold;
`

const Range = styled.p`
    color: ${ props => props.color };
    font-size: 12px;
    margin: 0;
`