import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components"

const propTypes = {};

const defaultProps = {};

export default class RadioInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <Wrapper>
                <Text>Tipo de escola que cursou:</Text>
                <Form>
                    <Label>
                        <HiddenInput
                            {...this.props}
                        />
                        <Circle
                            
                        />
                    </Label>
                </Form>
            </Wrapper>
        );
    }
}

 RadioInput.propTypes = propTypes;
 RadioInput.defaultProps = defaultProps;

 const Wrapper = styled.div`
    width: 100%;

 `

 const Text = styled.p`
    font-size: 16px;
    color: #2b2d5c;
 `

 const Form = styled.div`

 `

 const Label = styled.label`
    display: flex;
    align-items: center;
    color: #6c6c6c;
 `

 const HiddenInput = styled.input`
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
 `

 const Circle = styled.span`
    width: 20px;
    height: 20px;
    border-radius: 100%;
`