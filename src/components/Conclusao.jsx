import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components"
import { DataConsumer } from "../contexts/DataProvider"
import { LeadDataConsumer } from "../contexts/LeadDataProvider"
import Drawer from "./Drawer"
import { ThemeConsumer } from "../contexts/ThemeProvider"
import Button from "./Button"
import {SendDataConsumer} from "../contexts/SendDataProvider"
import { isMobileOnly } from "react-device-detect"

const propTypes = {};

const defaultProps = {};

export default function Conclusao(props) {
    function renderItems({ inscricao }, leadData, theme) {
        return inscricao.map( i => {
            // Renderiza o item apenas se o dado existir. Ajuda com paramteros que são opcionais.
            if(leadData[i.param]) {
                return (
                    <ListItem>
                            <Span color={theme.text}>></Span>{ i.info }: <Span color={theme.text} style={{ textTransform: "uppercase" }}> { leadData[i.param]} </Span>
                    </ListItem>
                )
            }
            return null
        })
    }
    function renderChamada(leadData, conclusao, theme) {
        if( process.env.REACT_APP_IES === "fmu" ) {
            if( parseInt(leadData.notaEnem, 10) < 200 ) {
                return(
                    <React.Fragment>
                        <Text style={{fontSize: '20px', textAlign: 'center', marginBottom: '0'}}>
                            <div><strong>Fique Tranquilo!</strong></div>
                            <span>Sua nota no ENEM foi {leadData.notaEnem}, mas você pode fazer o Vestibular Agendado</span>
                        </Text>
                        <Button>
                            <Button.Agendamento text="Agendar Vestibular" actions={[ "href", conclusao.botao.action, "dataLayerEvent" ]} />
                        </Button>
                    </React.Fragment>
                )
            }
            return(
                <Text style={{textAlign: 'center'}}>Os seus dados estão sendo processados. <a href="https://movemais.fmu.br/suainscricao/">Clique aqui</a> e retorne em alguns minutos e faça sua matrícula!</Text>
            )
        }
        if(leadData.agendado) return(
            <React.Fragment>
                <Text>Dados de Agendamento</Text>

                <div style={{width: "100%", textAlign: "center", display: "flex"}}>
                    <TextBlock>
                        <strong>Data: {leadData.agendamentoData}</strong><br /><br />
                        Horário de atendimento <br />
                        { conclusao.campus.filter(c => c.name == leadData.agendamentoCampus)[0].horario }
                    </TextBlock>
                    <TextBlock>
                        <strong>{leadData.agendamentoCampus}</strong><br />
                        { conclusao.campus.filter(c => c.name == leadData.agendamentoCampus)[0].endereco }
                        <br /><br />
                        Telefone: { conclusao.campus.filter(c => c.name == leadData.agendamentoCampus)[0].telefone }
                    </TextBlock>
                </div>
                <Bla>{ conclusao.agendamento && conclusao.agendamento.mensagem }</Bla>
            </React.Fragment>
        ); else return(
            <React.Fragment>
                { !isMobileOnly && <Text>{ conclusao.chamada }</Text> }
                <Drawer 
                    text={conclusao.botao.texto} 
                    marginTop="0" 
                    renderDataAs="final" 
                    theme={theme} 
                    name="valueCampusPolo"
                    render="button"
                    noSelect 
                    showLoading={false}
                />
            </React.Fragment>
        );
    }

    return (
        <React.Fragment>
            <DataConsumer>
                {({ conclusao }) => (
                    <SendDataConsumer>
                        {({ id }) => (
                            <LeadDataConsumer>
                                {({ leadData, updateLeadData }) => {
                                    !leadData.id && updateLeadData({ name: "id", value: id })
                                    return(
                                        <ThemeConsumer>
                                            {theme => (
                                                <React.Fragment>
                                                    <List>
                                                        {renderItems(conclusao, leadData, theme)}
                                                    </List>
                                                    {renderChamada(leadData, conclusao, theme)}
                                                </React.Fragment>
                                            )}
                                        </ThemeConsumer>
                                    )
                                }}
                            </LeadDataConsumer>
                        )}
                    </SendDataConsumer>
                )}
            </DataConsumer>
        </React.Fragment>
    );
}

Conclusao.propTypes = propTypes;
Conclusao.defaultProps = defaultProps;

const List = styled.ul`
    width: auto;
    max-width: 70%;
    margin: auto;
    padding: 0;
    margin: 0;
    list-style: none;
    margin-top: 40px;
`

const ListItem = styled.li`
    width: auto;
    color: #797979;
    margin-bottom: 35px;
    font-size: 13PX;

    &:last-of-type {
        margin-bottom: 0;
    }
`

const Span = styled.span`
    color: ${ props => props.color ? props.color : "#d10a11" };
    font-size: 13PX;
`

const Text = styled.p`
    font-size: 25px;
    color: #000;
    margin-top: 50px;
    margin-bottom: 35px;
`

const Bla = styled.p`
    color: #000;
    font-size: 18px;
    width: 100%;
    text-align: center;
    padding: 20px 0 40px 0;
`

const TextBlock = styled.div`
    width: 230px;
    margin-right: 30px;
    display: inline-block;
    text-align: left;
    max-width: 50%;
    border-right: 1px solid #000;
    flex: 1;
    &:last-child {
        border-right: 0;
        margin-right: 0;
    }
`