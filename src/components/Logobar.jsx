import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components"
import { DataConsumer } from "../contexts/DataProvider"

const propTypes = {
    /** Referência para o endereço do logo. */
    logo: PropTypes.string.isRequired
};

const defaultProps = {};

require.context("../images/", true, /\-logo\.svg$/ )

/** Renderiza o logo centralizado em uma barra com background branco. */
export default function LogoBar({ logo, homePage }) {
    return (
        <React.Fragment>
            <Wrapper>
                <DataConsumer>
                    {({ basicData }) => (
                        <a href={basicData.url}>
                            <Logo src={require(`../images/${logo}`)} />  
                        </a>
                    )}
                </DataConsumer>
            </Wrapper>
        </React.Fragment>
    );
}

LogoBar.propTypes = propTypes;
LogoBar.defaultProps = defaultProps;

const Wrapper = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;

    @media (max-width: 600px) {
        margin: 30px 0;
    }

    a { display: block; }
`

const Logo = styled.img`
    @media (max-width: 600px) {
        width: 180px;
    }

    @media (min-width: 601px) {
        width: 300px;
        padding: 40px 0;
    }
`