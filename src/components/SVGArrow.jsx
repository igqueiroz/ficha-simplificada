import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components"

const propTypes = {
    /** Define a cor do ícone em SVG. */
    fill: PropTypes.string.isRequired,
    /** Define a direção do ícone. Padrão: direita. */
    direction: PropTypes.string,
    /** Define a largura do ícone. */
    width: PropTypes.string
};

const defaultProps = {};

export default function SVGArrow({ fill, width, direcao, position }) {
    return (
        <React.Fragment>
            <SVG position={position} direcao={direcao} xmlns="http://www.w3.org/2000/svg" width={width ? width : "10"} height="19" viewBox="0 0 10.831 19">
                <g id="arrow-point-to-right" transform="translate(-103.139 -1)">
                    <path fill={fill} id="Path_1277" data-name="Path 1277" d="M107.58,10.44,99.41,18.61a1.33,1.33,0,0,1-1.882-1.881L104.757,9.5,97.528,2.271A1.331,1.331,0,0,1,99.41.39l8.17,8.17a1.33,1.33,0,0,1,0,1.881Z" transform="translate(6 1)" fill="#d10a11"/>
                </g>
            </SVG>
        </React.Fragment>
    );
}

SVGArrow.propTypes = propTypes;
SVGArrow.defaultProps = defaultProps;

const SVG = styled.svg`
    margin-left: ${ props => props.marginLeft ? props.marginLeft : "0" };
    ${ props => props.position !== "relative" && "position: absolute" };
    right: ${ props => !props.position && props.direcao === "esquerda" ? "0" : "40px" };
    ${ props => props.direcao === "esquerda" && "transform: rotate(180deg);;" }
`