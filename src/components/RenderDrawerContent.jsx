import React from 'react';
import PropTypes from 'prop-types';
import AlphabeticalList from "./AlfabeticalList"
import Radio from "./Radio"
import FormAgendamento from "./FormAgendamento"
import Descontos from "./Descontos"
import LoadingSpinner from "./LoadingSpinner"

const propTypes = {};

const defaultProps = {};

export default function RenderDrawerContent(props) {
    function renderComponent({ renderDataAs }) {
        switch(renderDataAs) {
            case "list":
                return <AlphabeticalList {...props} />
            case "radio":
                return <Radio {...props} />
            case "final":
                return(
                    <React.Fragment>
                        <FormAgendamento />
                        <Descontos />
                    </React.Fragment>
                )
            default:
                return null
        }
    }

    function showLoading() {
        if(!props.showLoading) {
            return renderComponent(props)
        }
        if(props.data.length > 0) {
            return <LoadingSpinner />
        }
        return renderComponent(props)
    }

    return (
        <React.Fragment>
            {showLoading()}
        </React.Fragment>
    );
}

RenderDrawerContent.propTypes = propTypes;
RenderDrawerContent.defaultProps = defaultProps;