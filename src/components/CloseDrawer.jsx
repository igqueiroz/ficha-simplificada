import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components"
import SVGArrow from "./SVGArrow"

const propTypes = {};

const defaultProps = {};

export default function CloseDrawer(props) {
    return (
        <Wrapper {...props}>
            <SVGArrow position="relative" />
        </Wrapper>
    );
}

CloseDrawer.propTypes = propTypes;
CloseDrawer.defaultProps = defaultProps;

const Wrapper = styled.div`
    @media (max-width:600px)
        height: 20px;
        width: 20px;
    }

    @media (min-width:601px) {
        width: 40px;
        height: 40px;
    }
    position: fixed;
    top: 40px;
    background: #fff;
    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
    border-radius: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
`