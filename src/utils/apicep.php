<?php

if(isset($_GET['cep'])) {

    $cep = str_replace('-', '', $_GET['cep']); // o cep!

    $url = "https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente?wsdl";

    $xmlRequest = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
        xmlns:cli="http://cliente.bean.master.sigep.bsb.correios.com.br/">
        <soapenv:Header/>
        <soapenv:Body>
        <cli:consultaCEP>
        <cep>'.$cep.'</cep>
        </cli:consultaCEP>
        </soapenv:Body>
    </soapenv:Envelope>';

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml;charset=UTF-8'));
    curl_setopt($ch, CURLOPT_POSTFIELDS, "$xmlRequest");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($ch);
    curl_close($ch);

    $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $output);
    $xml = simplexml_load_string($xml);
    $json = json_encode($xml);
    $responseArray = json_decode($json,true);

    $bodyArray = $responseArray["soapBody"]["ns2consultaCEPResponse"]["return"];

    $rua = $bodyArray["end"];
    $bairro = $bodyArray["bairro"];
    $cidade = $bodyArray["cidade"];
    $estado = $bodyArray["uf"];

    $arr = array(
        "rua" => $rua, 
        "bairro" => $bairro,
        "cidade" => $cidade,
        "estado" => $estado
    );

    echo json_encode($arr, JSON_UNESCAPED_UNICODE);

}

die();