export const getGTMCode = ie => {
    switch( ie ) {
        case 'unn':
            return 'GTM-PZD9KG'
        case 'ufg':
            return 'GTM-N6XDLD'
        case 'fpb':
            return 'GTM-THX6XK'
        case 'unp':
            return 'GTM-TTV5TT'
        case 'unf':
            return 'GTM-5SNW97'
        case 'uam':
            return 'GTM-XWZN'
        case 'fmu':
            return "GTM-TTDJ2Z"
        case 'ibm':
            return 'GTM-WD9H3W'
        case 'fad':
            return 'GTM-5B2MH9'
        case 'unr':
            return 'GTM-PM7VVZ'
        case "ead":
            return 'GTM-M4Z5RW'
        default:
            throw new Error("Erro ao pegar RD. IES não identificada.")
    };
}

/** Dispara o evento de  */
export const dataLayerEvent = (step, callback, condition=false) => {
    if(process.env.NODE_ENV !== "development" ) {
        window.dataLayer.push({
            'event': 'etapa',
            'etapa': step
        });
        condition && callback({ name: "hasOpenedDrawer", value: true })
    }
}