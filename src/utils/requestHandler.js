import axios from "axios"

let baseURL
if (process.env.NODE_ENV === "development") {
    baseURL = `http://localhost:8010/enem-225813/us-central1/api/${process.env.REACT_APP_IES}`
}
else  {
    baseURL = `https://us-central1-enem-225813.cloudfunctions.net/api/${process.env.REACT_APP_IES}`
}

export default axios.create({
    baseURL,
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
    }
})