export function normalizeData(originalData) {
    let { cpf, telefone, cep, nascimento } = originalData
    let finalData = originalData
    if( cpf ) {
        const cpfAjustado = cpf.replace(/[^\w\s]/gi, '')
        finalData = { ...finalData, cpf: cpfAjustado }
    }
    if( telefone ) {
        const telAjustado = telefone.replace( "(", "" ).replace( ")", "" ).replace( "-", "" ).replace( " ", "" ).replace( " ", "" )
        finalData = { ...finalData, telefone: telAjustado }
    }
    if( cep ) {
        const cepAjustado = cep.replace( ".", "" ).replace( "-", "" )
        finalData = { ...finalData, cep: cepAjustado }
    }
    if( nascimento ) {
        const nascimentoAjustado = nascimento.replace( "/", "" ).replace( "/", "" )
        finalData = { ...finalData, nascimento: nascimentoAjustado }
    }
    const utm_source = localStorage.getItem('utm_source') || null;
    finalData = { ...finalData, utm: utm_source }
    return finalData
}