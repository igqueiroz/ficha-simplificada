import axios from "axios"

const error = {
    hasError: true,
    errorMsg: "Ops! Preenchimento incorreto!",
    showErrorBar: false
}

const success =  {
    hasError: false,
    errorMsg: "",
    showErrorBar: false
}

export default {
    nome(value) {
        if( value.length < 4 ) {
            return error
        }
        if( !value.trim().includes(" ") ) {
            return error
        }
        return success
    },

    telefone(value) {
        const telefone = value.replace(/[^\w\s_]/gi, '').replace(/\s/g, '');
        const ddd = telefone.substring(0, 2)
        const numeroSemDDD = telefone.substring(2, 11)

        if(telefone.length !== 11) {
            return error
        }

        if(/^\D*(\d)(?:\D*|\1)*$/.test(telefone.substring(3, 10))) {
            return error
        }

        if(numeroSemDDD.substring(0, 1) == 0 || numeroSemDDD.substring(0, 1) != 9 || ddd.substring(0, 1) == 0) {
            return error
        }

        if(numeroSemDDD[0] != 9) {
            return error
        }

        return success
    },

    email(value) {
        const regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        if( !regex.test(value) ) {
            return error
        }
        return success
    },

    async cpf(value) {
        const mod11 = num => num % 11 
        const NOT = x => !x
        const isEqual = a => b => b === a 
        const mergeDigits = ( num1, num2 ) => `${num1}${num2}`
        const getTwoLastDigits = cpf => `${cpf[ 9 ]}${cpf[ 10 ]}`
        const getCpfNumeral = cpf => cpf.substr( 0, 9 ).split( '' )

        const isRepeatingChars = str => 
        str.split( '' ).every( ( elem ) => elem === str[ 0 ] )

        const toSumOfProducts = ( multiplier ) => ( result, num, i ) => 
        result + ( num * multiplier-- )

        const getSumOfProducts = ( list, multiplier ) => 
        list.reduce( toSumOfProducts( multiplier ), 0 )

        const getValidationDigit = ( multiplier ) => ( cpf ) =>
        getDigit( mod11( getSumOfProducts( cpf, multiplier ) ) )

        const getDigit = ( num ) => 
        ( num > 1 )
            ? 11 - num
            : 0

        const isRepeatingNumbersCpf = isRepeatingChars

        const isValidCPF = ( cpf ) => {
            const CPF = getCpfNumeral( cpf )
            const firstDigit = getValidationDigit( 10 )( CPF )
            const secondDigit = getValidationDigit( 11 )( CPF.concat( firstDigit ) )
            return isEqual( getTwoLastDigits( cpf ) )
                            ( mergeDigits( firstDigit, secondDigit ) )
        }

        const validate = ( CPF ) => NOT( isRepeatingNumbersCpf( CPF ) ) && isValidCPF( CPF )
        var responseObj;

        if(validate( value.replace(/[^\w\s]/gi, '') )) {
            return await axios.get(`https://us-central1-enem-225813.cloudfunctions.net/api/valid/${value.replace(/[^\w\s]/gi, '')}`)
                .then( res => {
                    const { inscrito } = res.data
                    if( !inscrito ) return success;
                    return {
                        hasError: true,
                        errorMsg: "",
                        showErrorBar: true
                    };
                })
        }
        return error
    },

    notaEnem(value) {
        if(value.length == 0) {
            return error
        }
        return success
    },

    notaRedacao(value) {
        if(value.length == 0) {
            return error
        }
        return success
    },

    anoEnem(value) {
        const ano = parseInt(value)
        if( ano < 2000 ) {
            return error
        }
        return success
    },

    nomeMae(value) {
        if(value.length < 5 || !value.trim().includes(" ")) {
            return error
        }
        return success
    },

    rg(value) {
        if(value.length < 5 ) {
            return error
        }
        return success
    },

    nascimento(value) {
        const dateToArray = value.split("/")
        const day = dateToArray[0]
        const month = dateToArray[1]
        const year = dateToArray[2]

        const today = new Date().setHours(0,0,0,0)
        const rawDate = new Date( year , month , day ).setHours(0,0,0,0)

        if (day > 31 || month > 12 || rawDate > today || year < 1950 || year > 2003) {
            return error
        }
        
        return success
    },

    anoRealizacao(value) {
        if( value.length < 4 ) {
            return error
        }
        if( value > 2018 ) {
            return error
        }
        return success
    },

    async cep(value) {
        const cep = value.replace(/[^\w\s]/gi, '')
        var responseObj;
        
        if(cep.length < 8) {
            return error
        }
        
        return await axios.get(`https://enem.fmu.br/api/apicep.php?cep=${cep}`)
        .then( res => {
            if(res.data.rua != null) responseObj = success;
            else responseObj = error;
            return responseObj
        })
    },

    numero(value) {
        if(value.length === 0) {
            return error
        }
        return success
    },

    escola(value) {
        if(value.length === 0 ) {
            return error
        }
        return success
    }
}