import React, { Component } from 'react';
import Root from "./components/Root"
import Provider from "./contexts/AppProvider"
import { createGlobalStyle } from "styled-components"
import { isMobileOnly } from "react-device-detect"
import TagManager from 'react-gtm-module'
import { getGTMCode } from "./utils/gtm.js"

const GlobalStyle = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Lato');
  html, body {
    height: 100%;
  }
  body {
    font-family: 'Lato', sans-serif;
    overflow-x: hidden;
  }
  #root {
    min-height: 100%;
    display: flex;
    flex-wrap: wrap;
    @media (max-width:600px) {
      flex-direction: column;
    }
  }
`
if(process.env.NODE_ENV !== "development") {
  const tagManagerArgs = {
    gtmId: getGTMCode(process.env.REACT_APP_IES)
  }
  TagManager.initialize(tagManagerArgs)
}

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <GlobalStyle />
        <Provider>
          <Root />
        </Provider>
      </React.Fragment>
    );
  }
}

export default App;
